﻿# Parent class Person
class Person
{
    [int] $SocialId;
    [string] $FirstName;
    [string] $LastName;

    Person([int] $SocialId, [string] $FirstName, [string] $LastName)
    {
        $this.SocialId = $SocialId
        $this.FirstName = $FirstName
        $this.LastName = $LastName
    }

    [string] SetToString()
    {
        throw("Override the parent function !")
    }

    [void] ToString()
    {
        Write-Host $this.SetToString()
    }
}

# Child class Manager extends Person
class Manager : Person
{
    [int] $ProjectsInvolved;
    [double] $Bonus;

    Manager([int] $SocialId, [string] $FirstName, [string] $LastName, [int] $ProjectsInvolved, [double] $Bonus) : base($SocialId, $FirstName, $LastName)
    {
        $this.ProjectsInvolved = $ProjectsInvolved
        $this.Bonus = $Bonus
    }

    [string] SetToString()
    {
        return "Manager: " + $this.SocialId + " - " + $this.FirstName + " - " + $this.LastName + " - " + $this.ProjectsInvolved + " - " + $this.Bonus
    }
}

# Child class Engenier extends Person
class Engenier : Person
{
    [int] $TasksDone;
    [double] $WorkingHours;

    Engenier([int] $SocialId, [string] $FirstName, [string] $LastName, [int] $TasksDone, [double] $WorkingHours) : base($SocialId, $FirstName, $LastName)
    {
        $this.TasksDone = $TasksDone
        $this.WorkingHours = $WorkingHours
    }

    [string] SetToString()
    {
        return "Engenier: " + $this.SocialId + " - " + $this.FirstName + " - " + $this.LastName + " - " + $this.TasksDone + " - " + $this.WorkingHours
    }
}

# Arraylist for employees
$EmployeesList = New-Object System.Collections.ArrayList

# Creating new employees and populating the list
$EmployeesList.Add([Manager]::new(4592, "FirstName 1", "LastName 1", 12, 22.0)) | Out-Null
$EmployeesList.Add([Manager]::new(4532, "FirstName 2", "LastName 2", 6, 34.0)) | Out-Null
$EmployeesList.Add([Engenier]::new(3132, "FirstName 3", "LastName 3", 4, 13.22)) | Out-Null
$EmployeesList.Add([Engenier]::new(6732, "FirstName 4", "LastName 4", 8, 14.18)) | Out-Null

# Output EmployeesList
foreach ($Employee in $EmployeesList) {
    Write-Host $Employee.ToString()
}