﻿# Connect to database
function ConnectToDatabase([string] $user, [string] $pass, [string] $MySQLHost, [string] $database) { 
    [void][system.reflection.Assembly]::LoadWithPartialName("MySql.Data") 
 
    # Open Connection 
    $connStr = "server=" + $MySQLHost + ";port=3306;uid=" + $user + ";pwd=" + $pass + ";database=" + $database + ";Pooling=FALSE" 

    try {
        $conn = New-Object MySql.Data.MySqlClient.MySqlConnection($connStr) 
        $conn.Open()
    } catch [System.Management.Automation.PSArgumentException] {
       Write-Host "Unable to connect to MySQL server, do you have the MySQL connector installed..?"
       exit
    } catch {
       Write-Host "Unable to connect to MySQL server..."
        exit
    }

    Write-Host "Connected to MySQL database $MySQLHost\$database"

    return $conn 
}

# Execute query
function ExecuteQuery([string] $query, $conn) { 
  $cmd = New-Object MySql.Data.MySqlClient.MySqlCommand($query, $conn)    
  $dataAdapter = New-Object MySql.Data.MySqlClient.MySqlDataAdapter($cmd)      
  $dataSet = New-Object System.Data.DataSet                                   
  $dataAdapter.Fill($dataSet, "data")                                                  
  $cmd.Dispose()

  return $dataSet.Tables["data"]                                               
}

# Connection variables 
$user = 'root' 
$pass = '' 
$database = 'accounting_department' 
$MySQLHost = 'localhost' 

# Connect to MySQL Database 
$conn = ConnectToDatabase $user $pass $MySQLHost $database

$query = "SELECT * FROM suppliers;"
$results = ExecuteQuery $query $conn

Write-Host "Found: " + $results.rows.count + " rows..."

$results | Format-Table