#include <iostream>
#include <array>

// O(N^2)
void bubbleSort(std::array<int, 6> &arr)
{
    int length = arr.size() - 1;

    for (int i = 0; i < length; i++)
    {
        // 3, 6, 4, 1, 2, 5
        for (int j = 0; j < length; j++)
        {
            if (arr[j] > arr[j + 1])
            {
                int temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
}

// O(N^2)
void selectionSort(std::array<int, 6> &arr)
{
    int min = 0, length = arr.size();

    // 3, 6, 4, 1, 2, 5
    for (int i = 0; i < length - 1; i++)
    {
        min = i;
        for (int j = i + 1; j < length; j++)
        {
            if (arr[j] < arr[min])
            {
                min = j;
            }
        }

        int temp = arr[min];
        arr[min] = arr[i];
        arr[i] = temp;
    }
}

// O(N^2)
void insertionSort(std::array<int, 6> &arr)
{
    int length = arr.size(), insert = 0, slot = 0;

    // 3, 6, 4, 1, 2, 5
    for (int i = 1; i < length; i++)
    {
        insert = arr[i];
        slot = i;

        while (slot > 0 && arr[slot - 1] > insert)
        {
            arr[slot] = arr[slot - 1];
            slot = slot - 1;
        }

        arr[slot] = insert;
    }
}

// O(N log(N))
void quickSort(std::array<int, 8> &arr, int leftI, int rightI)
{
    int left = leftI, right = rightI,
        pivot = arr[leftI + (rightI - leftI) / 2];

    if (leftI >= rightI)
    {
        return;
    }

    //3, 6, 4, 8, 1, 7, 2, 5
    while (left <= right)
    {
        while (arr[left] < pivot)
        {
            left++;
        }

        while (arr[right] > pivot)
        {
            right--;
        }

        if (left <= right)
        {
            int temp = arr[left];
            arr[left] = arr[right];
            arr[right] = temp;

            left++;
            right--;
        }
    }

    quickSort(arr, leftI, right);
    quickSort(arr, left, rightI);
}

int partition(std::array<int, 8> &arr, int left, int right)
{
    //3, 6, 4, 8, 1, 7, 2, 5
    int pivot = arr[right], i = (left - 1);

    for (int j = left; j < right; j++)
    {
        if (arr[j] < pivot)
        {
            i++;
            int temp = arr[j];
            arr[j] = arr[i];
            arr[i] = temp;
        }
    }

    int temp = arr[right];
    arr[right] = arr[i + 1];
    arr[i + 1] = temp;

    return i + 1;
}

// O(N log(N))
void quickSortRight(std::array<int, 8> &arr, int left, int right)
{
    if (left < right)
    {
        int pi = partition(arr, left, right);

        quickSortRight(arr, left, pi - 1);
        quickSortRight(arr, pi + 1, right);
    }
}

int main()
{
    std::cout << "==== Bubble Sort ====" << std::endl;
    std::array<int, 6> arr{3, 6, 2, 4, 1, 5};
    bubbleSort(arr);

    for (int i = 0; i < arr.size(); i++)
    {
        std::cout << arr[i] << std::endl;
    }

    std::cout << "==== Selection Sort ====" << std::endl;
    std::array<int, 6> arr3{3, 6, 4, 1, 2, 5};
    selectionSort(arr3);

    for (int i = 0; i < arr3.size(); i++)
    {
        std::cout << arr3[i] << std::endl;
    }

    std::cout << "==== Insertion Sort ====" << std::endl;
    std::array<int, 6> arr2{3, 6, 2, 4, 1, 5};
    insertionSort(arr2);

    for (int i = 0; i < arr2.size(); i++)
    {
        std::cout << arr2[i] << std::endl;
    }

    std::cout << "==== Quick Sort Middle ====" << std::endl;
    std::array<int, 8> arr4{3, 6, 4, 8, 1, 7, 2, 5};
    quickSort(arr4, 0, arr4.size() - 1);

    for (int i = 0; i < arr4.size(); i++)
    {
        std::cout << arr4[i] << std::endl;
    }

    std::cout << "==== Quick Sort Right Pivot ====" << std::endl;
    std::array<int, 8> arr5{3, 6, 4, 8, 1, 7, 2, 5};
    quickSortRight(arr5, 0, arr5.size() - 1);

    for (int i = 0; i < arr5.size(); i++)
    {
        std::cout << arr5[i] << std::endl;
    }

    return 0;
}