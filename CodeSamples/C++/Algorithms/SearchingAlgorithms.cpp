#include <iostream>
#include <array>

// Run time complexity O(N) and complexity O(1)
void linearSearch(std::array<int, 11> &arr, int value)
{
    for (int i = 0; i < arr.size() - 1; i++)
    {
        if (arr[i] == value)
        {
            std::cout << "value is : " << arr[i] << std::endl;
            break;
        }
    }
}

// Run time complexity O(log(N)) and complexity O(1)
// worst case can be O(N)
void binarySearch(std::array<int, 11> &arr, int value)
{
    int left = 0, right = arr.size() - 1;

    // 1, 2, 3, 4, 5, 6
    while (left <= right)
    {
        int mid = left + (right - left) / 2;

        if (arr[mid] == value)
        {
            std::cout << "value is : " << arr[mid] << std::endl;
            break;
        }
        else if (arr[mid] < value)
        {
            left = mid + 1;
        }
        else if (arr[mid] > value)
        {
            right = mid - 1;
        }
    }
}

// Run time complexity O(log log(N)) and complexity O(1)
// worst case can be O(N)
void interpolationSearch(std::array<int, 11> &arr, int value) // 5
{
    int left = 0, right = arr.size() - 1;
    // 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11
    while (left <= right && value >= arr[left] && value <= arr[right])
    {
        if (left == right)
        {
            if (arr[left] == value)
            {
                std::cout << "value is : " << arr[left] << std::endl;
            }

            break;
        }

        int pos = left + (((right - left) / (arr[right] - arr[left])) * (value - arr[left]));
        // 0 + ((10 - 0) / (11 - 1)) * (4 - 1) =  0 + 1 * 3 = 3

        if (arr[pos] == value)
        {
            std::cout << "value is : " << arr[pos] << std::endl;
            break;
        }
        else if (arr[pos] < value)
        {
            left = pos + 1;
        }
        else if (arr[pos] > value)
        {
            right = pos - 1;
        }
    }
}

int main()
{
    std::array<int, 11> arr{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};

    std::cout << "==== Linear Search ====" << std::endl;
    linearSearch(arr, 3);
    std::cout << "==== Binary Search ====" << std::endl;
    binarySearch(arr, 5);
    std::cout << "==== Interpolation Search ====" << std::endl;
    interpolationSearch(arr, 4);

    return 0;
}