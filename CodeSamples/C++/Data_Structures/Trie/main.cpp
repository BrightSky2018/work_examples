#include <iostream>
#include <vector>
#include "trie.h"

int main()
{
    std::vector<std::string> inputs = {"andrey", "andreylot", "andromeda", "anton", "mikhail", "miraslav"};
    Trie *root = new Trie();

    for (int i = 0; i < inputs.size(); i++)
    {
        root->insert(inputs[i]);
    }

    root->remove("anton");
    root->remove("miraslav");

    //remove(root, "anton");
    root->search("andrey") ? std::cout << "found\n" : std::cout << "not found\n";
    root->search("andreylot") ? std::cout << "found\n" : std::cout << "not found\n";
    root->search("andromeda") ? std::cout << "found\n" : std::cout << "not found\n";
    root->search("anton") ? std::cout << "found\n" : std::cout << "not found\n";
    root->search("mikhail") ? std::cout << "found\n" : std::cout << "not found\n";
    root->search("miraslav") ? std::cout << "found\n" : std::cout << "not found\n";

    return 0;
}