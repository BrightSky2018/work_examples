#ifndef TRIE_H
#define TRIE_H

// Characters size
const int SIZE = 26;

// Trie node
struct TrieNode
{
    /** struct property - an array to store children */
    struct TrieNode *child[SIZE];
    bool endOfString;
};

/**
   @brief C++ trie implementation using TrieNode with array.
   Trie data structure similar to the Tree data structure, for storing letters/prefixies/words and allows to look
   up words formed from the letters. Commonly used in the words validation problems. Tries start with the root node
   and stores each new data as a new node connected to the root. 
   Each node has capacity to store N number of node pointers in array, where N usually correspond to an
   alphabet size can be 26, 23 more or less letters and depend on the language. When a new node is created
   it is stored on position of alphabetical order and does not allow duplicates.

   Trie *chars[26] = [a, b, c, d....] 
                           /
                          /
   Trie *chars[26] = [a, b, c, d....]       

 */
// StackLL class declaration
class Trie
{
private:
    /** class property - Root node */
    TrieNode *root = nullptr;

public:
    /**
        @brief  Base constructor.
       
        If needed, can have other constructors for move and copy
     */
    Trie();

    /**
        @brief  Base destructor.
   
        Calls destructor and deletes stack elements.
     */
    ~Trie();

    /**
        Initializes and returns a new TrieNode node.

	    @return TrieNode.
    */
    TrieNode *createNode();

    /**
        Adds new string to the tree.

        @param string key - new value to insert.
	    @return void.
    */
    void insert(std::string key);

    /**
        Searches for a specific string in the tree and returns bool true/false.

        @param string key - value to search for.
	    @return bool.
    */
    bool search(std::string key);

    /**
        Removes specified string from the tree.

        @param string key - value to be removed.
	    @return void.
    */
    void remove(std::string key);

    /**
        Checks if specified node has no entries (empty) in the belonging child array and returns bool true/false.

        @param TrieNode *root - a node whose child array to check.
	    @return bool.
    */
    bool isEmpty(TrieNode *root);

    /**
        Adds new node after specified element in the list.

	    @return TrieNode *root.
    */
    TrieNode *getRoot();
};

#endif

#include <iostream>
#include "../Stack/stack_linked_list.h"

Trie::Trie()
{
    root = createNode();
}

Trie::~Trie()
{
    // disctructor
}

TrieNode *Trie::createNode()
{
    TrieNode *trieNode = new TrieNode;
    trieNode->endOfString = false;

    for (int i = 0; i < SIZE; i++)
    {
        trieNode->child[i] = NULL;
    }

    return trieNode;
}

void Trie::insert(std::string key)
{
    TrieNode *curr = this->root;

    for (int i = 0; i < key.length(); i++)
    {
        int index = key[i] - 'a';

        if (!curr->child[index])
        {
            curr->child[index] = createNode();
        }

        curr = curr->child[index];
    }

    curr->endOfString = true; //last node as leaf
}

bool Trie::search(std::string key)
{
    //check if key is present in trie. If present returns true
    TrieNode *curr = this->root;

    for (int i = 0; i < key.length(); i++)
    {
        int index = key[i] - 'a';

        if (!curr->child[index])
        {
            return false;
        }

        curr = curr->child[index];
    }

    return (curr != NULL && curr->endOfString);
}

bool Trie::isEmpty(TrieNode *root)
{
    for (int i = 0; i < SIZE; i++)
    {
        if (root->child[i])
        {
            return false;
        }
    }

    return true;
}

void Trie::remove(std::string key)
{
    StackLL<TrieNode *> trieNodes;
    TrieNode *temp = this->root;
    int index = 0;

    for (int i = 0; i < key.length(); i++)
    {
        index = key[i] - 'a';

        if (temp->child[index])
        {
            trieNodes.push(temp->child[index]);
        }

        temp = temp->child[index];
    }

    index = 0;

    while (trieNodes.size() > 0)
    {
        temp = trieNodes.pick();

        if (trieNodes.size() == key.length() && temp->endOfString)
        {
            temp->endOfString = false;

            if (!isEmpty(temp))
            {
                break;
            }
        }

        temp->child[index] = NULL;

        if (isEmpty(temp) && !temp->endOfString)
        {
            delete temp;
            temp = nullptr;
            index = key[trieNodes.size() - 1] - 'a';
        }

        trieNodes.pop();
    }
}

TrieNode *Trie::getRoot()
{
    return this->root;
}