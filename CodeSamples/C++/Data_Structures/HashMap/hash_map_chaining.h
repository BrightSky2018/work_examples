#ifndef HMC_H
#define HMC_H

// HM Node struct definition
template <typename K, typename V>
struct HashMapNode
{
    K key;
    V value;

    HashMapNode(K key, V value) : key(key), value(value) {}
    HashMapNode<K, V> *next;
};

/** 
    @brief C++ implementation of the hash map data structure. It is verstile and very fast,
    with most time performing on insert, search and delete at constant O(1) runtime, except
    collision cases, when the elements are chained and require traversal. At this point
    it can be either O(n) or O(log(n)). O(n) if elements chained via linked list and O(log(n))
    if a self balancing binary search tree is used. Usually, as in case with C++ STL map has
    Red Black Tree, thus the works case of the map performance is O(log(n)).

    Current implementation uses chaining (via linked list) to resolve hash key collision.
*/
// HM class definition
template <typename K, typename V>
class HashMapChaining
{
private:
    /** two integers represent capacity (_size) and number of elements inserted (_count) */
    int _size, _count;

    /** 
        pointer to the table of hashmap node pointers; 
        default pointers involved in various map operations
    */
    HashMapNode<K, V> **table, *temp{nullptr}, *prev{nullptr}, *entry{nullptr};

public:
    /**
        Hashmap default constructor, with an integer as map size.

        Initializes array of type T of a particular size.
     */
    HashMapChaining(int size);

    /**
        Base destructor.
   
        Calls destructor and deletes queue elements.
     */
    ~HashMapChaining();

    /**
       Generates hash key from K type value.

       @tparam K type key - value to generate hash key from.
       @return an integer value.
     */
    int generateHash(K key);

    /**
       Finds and returns hashtable Node value.

       @tparam K type key - entry value to retrieve
       @return V type value entry->value (success) || -1 (failure).
     */
    V get(K key);

    /**
       Inserts new <key, value> Node. key and value of any type, i.e - string, char, int, double, float.

       @tparam K type key - value to generate hash key to assosiate with stored record.
       @tparam V type value - data to be stored.
       @return void.
     */
    void insert(K key, V value);

    /**
       Removes a particular noded.

       @tparam K type key - value of entry under the corresponding key to be deleted.
       @return an integer - 0 || 1.
     */
    int remove(K key);

    /**
       Prints all stored map values.

       @return void.
     */
    void printMap();

    /**
       Returns number of elements stored in the map

       @return an integer value N.
     */
    int getCount();

    /**
       Returns boolean if map is empty or not.

       @return boolean value true || false.
     */
    bool isEmpty();
};

#endif

// HM class implementation
template <typename K, typename V>
HashMapChaining<K, V>::HashMapChaining(int size) : _size(size), _count(0)
{
    table = new HashMapNode<K, V> *[size] {};
}

template <typename K, typename V>
HashMapChaining<K, V>::~HashMapChaining()
{
    for (int i = 0; i < this->_size; i++)
    {
        if (this->table[i])
        {
            this->entry = this->table[i];

            while (this->entry)
            {
                this->prev = this->entry;
                this->entry = this->entry->next;
                delete this->prev;
            }
        }
    }

    delete[] this->table;
    this->prev = nullptr;

    std::cout << "Table destroyed" << std::endl;
}

template <typename K, typename V>
int HashMapChaining<K, V>::generateHash(K key)
{
    // for this particular example, c++ out of the box
    // hashing solution has been used, however if one
    // desires to use own hashing fucntion, has to keep
    // in mind, that will need several different functions
    // e.i int, char, string and etc.
    std::hash<K> h_key;
    return h_key(key) % this->_size;
}

template <typename K, typename V>
V HashMapChaining<K, V>::get(K key)
{
    int hash = this->generateHash(key);

    if (!this->table[hash])
    {
        return -1;
    }
    else
    {
        this->entry = this->table[hash];

        while (this->entry && this->entry->key != key)
        {
            this->entry = this->entry->next;
        }

        if (!this->entry)
        {
            return -1;
        }
        else
        {
            return this->entry->value;
        }
    }
}

template <typename K, typename V>
void HashMapChaining<K, V>::insert(K key, V value)
{
    //TODO: add hashing for stings and chars
    int hash = this->generateHash(key);

    if (!this->table[hash])
    {
        this->table[hash] = new HashMapNode<K, V>(key, value);
    }
    else
    {
        // hash key collision - chaining items in linked list can be replaced
        // with SBBST | RBTree, thus to improve runtime from O(N) to O(log N)
        this->entry = this->table[hash];

        while (this->entry->next)
        {
            this->entry = this->entry->next;
        }

        if (this->entry->key == key)
        {
            this->entry->value = value;
        }
        else
        {
            this->entry->next = new HashMapNode<K, V>(key, value);
        }
    }

    this->_count++;
}

template <typename K, typename V>
int HashMapChaining<K, V>::remove(K key)
{
    int hash = generateHash(key);

    if (!this->table[hash])
    {
        return 0;
    }

    this->entry = this->table[hash];

    while (this->entry->next && this->entry->key != key)
    {
        this->prev = this->entry;
        this->entry = this->entry->next;
    }

    if (this->entry->key == key)
    {
        if (!prev)
        {
            this->temp = this->entry->next;
            delete this->entry;
            this->table[hash] = this->temp;
        }
        else
        {
            this->temp = this->entry->next;
            delete this->entry;
            this->prev->next = this->temp;
        }

        this->_count--;
    }

    return 1;
}

template <typename K, typename V>
void HashMapChaining<K, V>::printMap()
{
    for (int i = 0; i < this->_size; i++)
    {
        if (this->table[i])
        {
            this->entry = this->table[i];

            while (this->entry)
            {
                std::cout << "Key : " << this->entry->key << " Value : " << this->entry->value << std::endl;
                this->entry = this->entry->next;
            }
        }
    }
}

template <typename K, typename V>
int HashMapChaining<K, V>::getCount()
{
    return this->_count;
}

template <typename K, typename V>
bool HashMapChaining<K, V>::isEmpty()
{
    return this->_count == 0;
}
