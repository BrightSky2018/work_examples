#include <iostream>
#include <algorithm>
#include "hash_map_chaining.h"
#include "hash_map_open_addressing.h"

int main()
{
    HashMapChaining<char, int> hmc(200);
    hmc.insert('A', 11);
    hmc.insert('B', 12);
    hmc.insert('C', 3);

    std::cout << "Collision data : " << std::endl;

    hmc.insert('D', 3);
    hmc.insert('E', 4);
    hmc.printMap();
    std::cout << "Delete item status : " << hmc.remove('E') << std::endl;

    std::cout << "Table values : " << std::endl;

    std::cout << hmc.get('A') << std::endl;
    std::cout << hmc.get('B') << std::endl;
    std::cout << hmc.get('C') << std::endl;
    std::cout << hmc.get('D') << std::endl;

    HashMapOpenAddressing<int, int> hmoa(200);
    hmoa.insert('A', 11);
    hmoa.insert('B', 12);
    hmoa.insert('C', 3);

    std::cout << "Collision data : " << std::endl;

    hmoa.insert('D', 3);
    hmoa.insert('E', 4);
    hmoa.printMap();
    std::cout << "Delete item status : " << hmoa.remove('E') << std::endl;

    std::cout << "Table values : " << std::endl;

    std::cout << hmoa.get('A') << std::endl;
    std::cout << hmoa.get('B') << std::endl;
    std::cout << hmoa.get('C') << std::endl;
    std::cout << hmoa.get('D') << std::endl;

    return 0;
}