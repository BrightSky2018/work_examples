#ifndef HMOA_H
#define HMOA_H

//template for generic type
template <typename K, typename V>
class HashMapOpenAddressing
{
private:
	int capacity;
	int count;

	HashMapNode<K, V> **table, *temp; // table can be implemented vector, list and array

public:
	explicit HashMapOpenAddressing(int size);
	~HashMapOpenAddressing();

	int generateHash(K key);
	void insert(K key, V value);
	int remove(K key);
	V get(K key);
	int getCount();
	bool isEmpty();
	void printMap();
};

#endif

template <typename K, typename V>
HashMapOpenAddressing<K, V>::HashMapOpenAddressing(int size) : capacity(size), table{nullptr}, temp{nullptr}, count(0)
{
	table = new HashMapNode<K, V> *[capacity] {};
}

template <typename K, typename V>
HashMapOpenAddressing<K, V>::~HashMapOpenAddressing()
{
	for (int i = 0; i < this->capacity; i++)
	{
		if (this->table[i])
		{
			delete this->table[i];
		}
	}

	delete[] this->table;
	std::cout << "Table destroyed" << std::endl;
}

template <typename K, typename V>
int HashMapOpenAddressing<K, V>::generateHash(K key)
{
	// for this particular example, c++ out of the box
	// hashing solution has been used, however if one
	// desires to use own hashing fucntion, has to keep
	// in mind, that will need several different functions
	// e.i int, char, string and etc.
	std::hash<K> h_key;
	return h_key(key) % this->capacity;
}

template <typename K, typename V>
void HashMapOpenAddressing<K, V>::insert(K key, V value)
{
	this->temp = new HashMapNode<K, V>(key, value);
	int hashIndex = generateHash(key);

	//resolve collision - find next free space
	while (this->table[hashIndex] && this->table[hashIndex]->key != key && this->table[hashIndex]->key != -1)
	{
		hashIndex++;
		hashIndex %= this->capacity;
	}

	//if new node to be inserted increase the current size
	if (!this->table[hashIndex] || this->table[hashIndex]->key == -1)
	{
		this->table[hashIndex] = this->temp;
		this->count++;
	}
}

template <typename K, typename V>
int HashMapOpenAddressing<K, V>::remove(K key)
{
	int hashIndex = generateHash(key);

	while (this->table[hashIndex])
	{
		if (this->table[hashIndex]->key == key)
		{
			this->table[hashIndex]->key = -1;
			this->table[hashIndex]->value = -1;
			this->count--;

			return 1;
		}

		hashIndex++;
		hashIndex %= this->capacity;
	}

	return 0;
}

template <typename K, typename V>
V HashMapOpenAddressing<K, V>::get(K key)
{
	int hashIndex = generateHash(key), counter = 0;

	while (this->table[hashIndex])
	{
		if (counter++ > this->capacity)
		{
			return 0;
		}

		if (this->table[hashIndex]->key == key)
		{
			return this->table[hashIndex]->value;
		}

		hashIndex++;
		hashIndex %= this->capacity;
	}

	return 0;
}

template <typename K, typename V>
int HashMapOpenAddressing<K, V>::getCount()
{
	return this->count;
}

template <typename K, typename V>
bool HashMapOpenAddressing<K, V>::isEmpty()
{
	return this->count == 0;
}

template <typename K, typename V>
void HashMapOpenAddressing<K, V>::printMap()
{
	for (int i = 0; i < this->capacity; i++)
	{
		if (this->table[i] && this->table[i]->key != -1)
		{
			std::cout << "Key " << this->table[i]->key << " Value " << this->table[i]->value << std::endl;
		}
	}
}
