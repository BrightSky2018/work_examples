#ifndef STACK_ARR_H
#define STACK_ARR_H

/**
   @brief C++ implementation of the stack data structure using array. 
   Its one of the options a stack can be built, along with linked list
   and std::vactor<>. Unlike the linked list implementation all elements
   of the stack occupy contiguous memory addresses.

   @tparam T Any type i.e int, double, flot, string char and etc.
 */
// Stack class definition
template <typename T>
class Stack
{
private:
    int front, arrSize;
    T *array;

public:
    /**
        Unlike on the linked list based stack, the array based stack 
        capacity has to be specified in advance, either from user 
        input or using compile time #define STACK_SIZE;
        
        Current stack capacity is set by a user and has explicit constructor,
        to avoid assigning values  Stack st = 3;
     */
    explicit Stack(int size);

    /**
        Base destructor.
   
        Calls destructor and deletes stack elements.
     */
    ~Stack();

    /**
        Adds a new element to the stack. LIFO algorithm.
   
        This does very little apart from providing a virtual base dtor.
     */
    void push(T value);

    /**
        Removes the very first element from the top of the stack.
   
        @return void.
     */
    void pop();

    /**
        Returns value of the very first element from the top of the stack.
   
        @return T type Node value.
     */
    T pick();

    /**
        Prints out all elements stored in the stack.
   
        @return void.
     */
    void printItems();

    /**
        Returns number of elements stored in the stack.
   
        @return an integer type value.
     */
    int size();
};

#endif // end of STACK_ARR_H

// Stack class implementation
template <typename T>
Stack<T>::Stack(int size) : arrSize(size), array(new T[size]), front(0)
{
}

template <typename T>
Stack<T>::~Stack()
{
    delete[] array;
}

template <typename T>
void Stack<T>::push(T value)
{
    if (this->front == this->arrSize)
    {
        return;
    }

    this->array[front] = value;
    this->front++;
}

template <typename T>
void Stack<T>::pop()
{
    if (this->front != 0)
    {
        this->front--;
    }
}

template <typename T>
T Stack<T>::pick()
{
    if (this->front == 0)
    {
        return 0;
    }

    return array[this->front - 1];
}

template <typename T>
int Stack<T>::size()
{
    return this->front;
}

template <typename T>
void Stack<T>::printItems()
{
    if (this->front == 0)
    {
        return;
    }

    for (int i = this->front - 1; i >= 0; i--)
    {
        std::cout << this->array[i] << std::endl;
    }
}
