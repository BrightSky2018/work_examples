#include <iostream>
#include "stack_array.h"
#include "stack_linked_list.h"

// Stack implementation via Array
// push O(1), pop O(1), pik O(1),  print O(N), mem. space O(N)
int main()
{
    Stack<double> sl(4);
    sl.push(1.2);
    sl.push(2.2);
    sl.push(3.2);
    sl.push(4.2);
    sl.push(5.2); // will not be pushed

    std::cout << "Size : " << sl.size() << std::endl;
    sl.pop();
    std::cout << "Peek : " << sl.pick() << std::endl;

    std::cout << "Before pop " << std::endl;
    sl.printItems();

    while (sl.size() != 0)
    {
        sl.pop();
    }

    sl.pop();
    std::cout << "Size : " << sl.size() << std::endl;

    sl.printItems();
    
    StackLL<double> s; // allocated on stack
    s.push(1.2);
    s.push(2.2);
    s.push(3.2);
    s.push(4.2);
    s.push(5.2); // will not be pushed

    std::cout << "Size : " << s.size() << std::endl;
    s.pop();
    std::cout << "Peek : " << s.pick() << std::endl;

    std::cout << "Before popping" << std::endl;
    s.printItems();

    while (s.size() != 0)
    {
        s.pop();
    }

    s.pop();
    std::cout << "Size : " << s.size() << std::endl;

    s.printItems();


    StackLL<char> sC; // allocated on stack
    sC.push('A');
    sC.push('B');
    sC.push('C');
    sC.push('D');
    sC.push('E');  // will not be pushed

    std::cout << "Size : " << sC.size() << std::endl;
    sC.pop();

    std::cout << "Before popping" << std::endl;
    sC.printItems();

    while (sC.size() != 0)
    {
        sC.pop();
    }

    std::cout << "Size : " << sC.size() << std::endl;

    sC.printItems();

    return 0;
}