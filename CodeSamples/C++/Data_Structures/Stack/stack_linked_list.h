#ifndef STACK_LL_H
#define STACK_LL_H

// Stack node
template <typename T>
struct SNode
{
    T data;
    SNode *next;

    SNode() : next(nullptr), data(0) {}
};

/**
   @brief C++ stack implementation using linked list.
   Unlike the linked list implementation all elements of the stack
   occupy different memory addresses.

   @tparam T Any type i.e int, double, flot, string char and etc.
 */
// StackLL class declaration
template <typename T>
class StackLL
{
private:
    /** integer to store number of elements in the stack */
    int count;

    /** pointers of type SNode */
    SNode<T> *head, *temp;

public:
    /**
        @brief  Base constructor.
       
        If needed, can have other constructors for move and copy
     */
    StackLL();

    /**
        @brief  Base destructor.
   
        Calls destructor and deletes stack elements.
     */
    ~StackLL();

    /**
        Adds a new element to the stack. LIFO algorithm.
   
        This does very little apart from providing a virtual base dtor.
     */
    void push(T value);

    /**
        Removes the very first element from the top of the stack.
   
        @return void.
     */
    void pop();

    /**
        Returns value of the very first element from the top of the stack.
   
        @return T type SNode value.
     */
    T pick();

    /**
        Returns number of elements stored in the stack.
   
        @return  An integer type value.
     */
    int size();

    /**
        Prints out all elements stored in the stack.
   
        @return void.
     */
    void printItems();
};

#endif // end of STACK_LL_H

// StackLL class implementation
template <typename T>
StackLL<T>::StackLL() : head(nullptr), temp(nullptr), count(0) {}

template <typename T>
StackLL<T>::~StackLL()
{
    while (this->head)
    {
        this->temp = this->head->next;
        delete this->head;
        this->head = this->temp;
    }

    head = nullptr;
}

template <typename T>
void StackLL<T>::push(T value)
{
    this->temp = new SNode<T>;
    this->temp->data = value;
    this->temp->next = this->head;
    this->head = this->temp;
    this->count++;
}

template <typename T>
void StackLL<T>::pop()
{
    if (this->head)
    {
        this->temp = this->head;
        //T data = temp->data;
        this->head = this->temp->next;
        delete this->temp;
        this->temp = nullptr;
        this->count--;
    }
}

template <typename T>
T StackLL<T>::pick()
{
    if (!this->head)
    {
        return 0;
    }

    return this->head->data;
}

template <typename T>
void StackLL<T>::printItems()
{
    if (!this->head)
    {
        return;
    }

    this->temp = this->head;

    while (this->temp)
    {
        std::cout << this->temp->data << std::endl;
        this->temp = this->temp->next;
    }
}

template <typename T>
int StackLL<T>::size()
{
    return this->count;
}
