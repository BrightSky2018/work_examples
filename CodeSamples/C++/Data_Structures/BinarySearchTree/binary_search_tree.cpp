#include <iostream>
#include "binary_search_tree.h"
#include "../Queue/queue_linked_list.h"
#include "../Stack/stack_linked_list.h"

BinarySearchTree::BinarySearchTree() : rootNode(nullptr)
{
}

BinarySearchTree::~BinarySearchTree()
{
}

Node *BinarySearchTree::getRoot()
{
    return this->rootNode;
}

void BinarySearchTree::insert(int value)
{
    //         12
    //        /  \
    //      10    14
    //     / \    / \
    //    9  11  13 15
    Node *newNode = new Node(value);

    if (!this->rootNode)
    {
        this->rootNode = newNode;
        return;
    }

    Node *current = this->rootNode;

    while (current)
    {
        if (current->data > value)
        {
            if (!current->leftNode)
            {
                current->leftNode = newNode;
                break;
            }
            else
            {
                current = current->leftNode;
            }
        }

        if (current->data < value)
        {
            if (!current->rightNode)
            {
                current->rightNode = newNode;
                break;
            }
            else
            {
                current = current->rightNode;
            }
        }
    }
}

int BinarySearchTree::remove(int value)
{
    //         12
    //        /  \
    //      10    14
    //     / \    / \
    //    9  11  13 15
    //                \
    //                18
    //               /
    //              17
    if (!this->rootNode)
    {
        return 0;
    }

    Node *current = this->rootNode;

    while (current)
    {
        if (current->data > value)
        {
            current = current->leftNode;
            continue;
        }

        if (current->data < value)
        {
            current = current->rightNode;
            continue;
        }

        if (current->data == value)
        {
            Node *parentNode = findParentNode(current->data);

            if (!current->leftNode && !current->rightNode)
            {
                if (current != this->rootNode)
                {
                    if (parentNode->leftNode == current)
                    {
                        parentNode->leftNode = nullptr;
                    }

                    if (parentNode->rightNode == current)
                    {
                        parentNode->rightNode = nullptr;
                    }
                }

                delete current;
                current = nullptr;
            }
            else if (current->leftNode && current->rightNode)
            {
                // due to binary nature of the tree the next
                // value closest to a deleting node has to be
                // on the right side and the most left node.
                // Since on the left side of a deleting node
                if (!current->rightNode->leftNode)
                {
                    current->data = current->rightNode->data;
                    current->rightNode = current->rightNode->rightNode;
                    return 1;
                }

                Node *replaceParent = nullptr;
                Node *replace = current->rightNode;

                while (replace->leftNode)
                {
                    replaceParent = replace;
                    replace = replace->leftNode;
                }

                current->data = replace->data;
                replaceParent->leftNode = replace->rightNode;
            }
            else
            {
                //         12
                //        /  \
                //      10    14
                //     / \    / \
                //    9  11  13 15
                //                \
                //                 18
                //               /
                //              17
                Node *child = current->leftNode ? current->leftNode : current->rightNode;

                if (current == this->rootNode)
                {
                    current = child;
                    return 1;
                }

                if (parentNode->leftNode == current)
                {
                    parentNode->leftNode = child;
                }

                if (parentNode->rightNode == current)
                {
                    parentNode->rightNode = child;
                }

                delete current;
                current = nullptr;
                //return 1;
            }
        }
    }

    return 1;
}

Node *BinarySearchTree::minValue(Node *root)
{
    Node *temp = root;

    while (temp)
    {
        temp = temp->leftNode;
    }

    return temp;
}

Node *BinarySearchTree::find(int value)
{
    if (!this->rootNode)
    {
        return nullptr;
    }

    Node *current = this->rootNode;

    while (current)
    {
        if (current->data > value)
        {
            current = current->leftNode;
        }
        else if (current->data < value)
        {
            current = current->rightNode;
        }
        else
        {
            return current;
        }
    }

    return nullptr;
}

// can be done both ways recursive and iterative
Node *BinarySearchTree::findParentNode(int value)
{
    //         12
    //        /  \
    //      10    14
    //     / \    / \
    //    9  11  13 15
    if (!this->rootNode || this->rootNode->data == value)
    {
        return nullptr;
    }

    Node *current = this->rootNode;

    while (current)
    {
        if (current->data > value)
        {
            if (current->leftNode && current->leftNode->data == value)
            {
                return current;
            }
            else
            {
                current = current->leftNode;
            }
        }
        if (current->data < value)
        {
            if (current->rightNode && current->rightNode->data == value)
            {
                return current;
            }
            else
            {
                current = current->rightNode;
            }
        }
    }

    return nullptr;
}

// Breadth first search is a typical search for the binary search tree
bool BinarySearchTree::breadthFirstSearch(int value)
{
    QueueLL<Node *> queue;
    Node *temp = this->rootNode;
    queue.enqueue(temp);

    //         12
    //        /  \
    //      10    14
    //     / \    / \
    //    9  11  13 15
    while (queue.size() > 0)
    {
        temp = queue.pick();
        queue.dequeue();

        if (temp->data == value)
        {
            return true;
        }

        if (temp->leftNode)
        {
            queue.enqueue(temp->leftNode);
        }

        if (temp->rightNode)
        {
            queue.enqueue(temp->rightNode);
        }
    }

    return false;
}

bool BinarySearchTree::depthFirstSearch(int value)
{
    StackLL<Node *> stack;
    Node *temp = this->rootNode;
    stack.push(temp);

    //         12
    //        /  \
    //      10    14
    //     / \    / \
    //    9  11  13 15
    while (stack.size() > 0)
    {
        temp = stack.pick();
        stack.pop();

        if (temp->data == value)
        {
            return true;
        }

        if (temp->rightNode)
        {
            stack.push(temp->rightNode);
        }

        if (temp->leftNode)
        {
            stack.push(temp->leftNode);
        }
    }

    return false;
}

// uses in-order traversal
bool BinarySearchTree::isValidBST()
{
    StackLL<Node *> stack;
    bool started = false;
    Node *temp = this->rootNode;
    int prevVal;

    //         12
    //        /  \
    //      10    14
    //     / \    / \
    //    9  11  13 15
    while (true)
    {
        if (temp)
        {
            stack.push(temp);
            temp = temp->leftNode;
            continue;
        }

        if (stack.size() == 0)
        {
            break;
        }

        temp = stack.pick();
        stack.pop();

        if (!started)
        {
            prevVal = temp->data;
            started = true;
        }
        else
        {
            if (prevVal > temp->data)
            {
                return false;
            }

            prevVal = temp->data;
        }

        temp = temp->rightNode;
    }

    return true;
}

bool BinarySearchTree::isBalanced(Node *root, int *height)
{
    int lh = 0, rh = 0, l = 0, r = 0;

    if (root == nullptr)
    {
        *height = 0;

        return 1;
    }

    l = isBalanced(root->leftNode, &lh);
    r = isBalanced(root->rightNode, &rh);

    *height = (lh > rh ? lh : rh) + 1;

    if (abs(lh - rh) >= 2)
    {
        return 0;
    }
    else
    {
        return l && r;
    }
}

int BinarySearchTree::getHeight(Node *root)
{
    if (!root)
    {
        return -1;
    }

    int left = 0, right = 0;

    left = getHeight(root->leftNode);
    right = getHeight(root->rightNode);

    return (left > right ? left : right) + 1;
}

void BinarySearchTree::preOrderTraverse(Node *root)
{
    if (root)
    {
        std::cout << root->data << std::endl;
        preOrderTraverse(root->leftNode);
        preOrderTraverse(root->rightNode);
    }
}

void BinarySearchTree::inOrderTraverse(Node *root)
{
    if (root)
    {
        inOrderTraverse(root->leftNode);
        std::cout << root->data << std::endl;
        inOrderTraverse(root->rightNode);
    }
}

void BinarySearchTree::postOrderTraverse(Node *root)
{
    if (root)
    {
        postOrderTraverse(root->leftNode);
        postOrderTraverse(root->rightNode);
        std::cout << root->data << std::endl;
    }
}
