#include <iostream>
#include "binary_search_tree.h"

int main()
{
    //         12
    //        /  \
    //      10    14
    //     / \    / \
    //    9  11  13 15
    BinarySearchTree *bst = new BinarySearchTree();
    int height = 0;

    // int arr[] {12, 10, 9, 11, 14, 15, 18, 17, 13};

    // for (auto x : arr)
    // {
    //     bst->insert(x);
    // }

    bst->insert(12);
    bst->insert(10);
    bst->insert(9);
    bst->insert(11);
    bst->insert(14);
    bst->insert(15);
    bst->insert(18);
    bst->insert(17);
    bst->insert(13);
    bst->insert(8);
    bst->insert(7);
    bst->insert(16);

    std::cout << "search " << bst->depthFirstSearch(15) << std::endl;
    bst->remove(15);
    bst->remove(12);
    std::cout << bst->depthFirstSearch(15) << std::endl;
    std::cout << "search " << bst->breadthFirstSearch(13) << std::endl;

    bst->inOrderTraverse(bst->getRoot());

    std::cout << "Is balanced bst " << bst->isBalanced(bst->getRoot(), &height) << std::endl;
    std::cout << " ====================================== \n";

    std::cout << "Found 9 " << bst->find(9) << std::endl;
    std::cout << "Parent node of 11 is " << bst->findParentNode(11) << std::endl;

    std::cout << "Delete node 16 " << bst->remove(16) << std::endl;
    std::cout << "Delete node 10 " << bst->remove(10) << std::endl;
    std::cout << "\n Tree after delete 9 and 10 nodes" << std::endl;
    bst->preOrderTraverse(bst->getRoot());
    std::cout << "\n -------" << std::endl;
    bst->inOrderTraverse(bst->getRoot());
    std::cout << "-----------------" << std::endl;
    std::cout << "Is valid bst " << bst->isValidBST() << std::endl;
    std::cout << "BST height is " << bst->getHeight(bst->getRoot()) << std::endl;

    return 0;
};