#ifndef BST_H
#define BST_H

struct Node
{
    /** an integer value of the Node. */
    int data;

    /** pointers to the left and right Nodes. */
    Node *rightNode, *leftNode;

    /**
        @brief  Base constructor.
     */
    Node(int value) : data(value), leftNode(nullptr), rightNode(nullptr) {}
};

/**
    @brief C++ implementation of the Binary Search Tree data structure.
    BST has several variations, such as AVL,  RBTree and Splay Tree.
    Some of them serve as a backbone for some data srtuctures map, set or as a 
    importnat part of OS in process scheduling (CFS Linux). Some variations as 
    B+Tree - used as the core for database (innoDB MySQL), or B-Tree for memory mapping.

    Current implementation is quite simple an just highlights the core idea
    of the BST data structure, which allows it to have log(N) runtime complexity.
    The idea behind log(N) runtime is a specific order in which elements are arranged,
    i.e the lowest element on the most left side, while the larges on the most right side.
    This allows to reduce on half standard insert, seach and delete operations, by
    comparing element against inserted and moving towards left or right side, depending
    of the comparison results.
    Essentially it is the same concept on which binary seach is based on.
 */
class BinarySearchTree
{
private:
    /** pointers to the root Node. */
    Node *rootNode;

public:
    /**
        Base constructor.
       
        If needed, can have other constructors for move and copy
     */
    BinarySearchTree();

    /**
        Base destructor.
   
        This does very little apart from providing a virtual base dtor.
     */
    ~BinarySearchTree();

    /**
        Does not return anything, just adds a new value.
        
        @return Pointer to the root node.
     */
    Node *getRoot();

    /**
        Does not return anything, just adds a new value.
        
        @param value an integer value to store in BST.
        @return void.
     */
    void insert(int value);

    /**
        Does not return anything, just deletes a specific value.
        
        @param value An integer value of a node to delete.
        @return void.
     */
    int remove(int value);

    /**
        Searches for the min value on the left side of a parent node.
        
        @param root Pointer to the root node.
        @return int value of a Node.
     */
    Node *minValue(Node *root);

    /**
        Does not return anything, just adds a new value.
        
        @param value an integer value of a node to find.
        @return bool.
     */
    Node *find(int value);

    /**
        Does not return anything, just adds a new value.
        
        @param value An integer value of a node whos parent to find.
        @return Node value.
     */
    Node *findParentNode(int value);

    /**
        Does not return anything, just adds a new value.
        
        @param value An integer value of a node to find.
        @return bool.
     */
    bool breadthFirstSearch(int value);

    /**
        Does not return anything, just adds a new value.
        
        @param value An integer value of a node to find.
        @return bool.
     */
    bool depthFirstSearch(int value);

    /**
        Prints BST values from rootNode, left subtree and 
        after right subtree
        
        @param root Pointer to the root node.
        @return void.
     */
    void preOrderTraverse(Node *root);

    /**
        Prints BST values from rootNode, left subtree and 
        after right subtree
        
        @param root Pointer to the root node.
        @return void.
     */
    void inOrderTraverse(Node *root);

    /**
        Prints BST values of left subtree, then right subtree
        and after rootNode
        
        @param root Pointer to the root node.
        @return void.
     */
    void postOrderTraverse(Node *root);

    /**
        Checks if BST is valid i.e fallows the rule
        left node < rootNode < right node
        
        @return bool.
     */
    bool isValidBST();

    /**
        Checks if BST is height balanced i.e, height difference
        between left and right subtrees is not larger than 1.
        
        @param root Pointer to the root node.
        @param value Pointer to the height integer to store BST levels.
        @return bool.
     */
    bool isBalanced(Node *root, int *height);

    /**
     * @brief Get height of the binary tree.
     * 
     * @param root Pointer to the root node.
     * @return int
     */
    int getHeight(Node *root);
};

#endif