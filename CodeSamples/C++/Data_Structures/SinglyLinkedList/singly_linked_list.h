#ifndef SLL_H
#define SLL_H

template <typename T>
struct Node
{
    T value;

    Node<T> *next;
    Node() : next(nullptr), value(0) {}
};

/**
    C++ implementation of singly linked list data structure.

	@tparam T any value float, double, long double, char, string and etc.
 */
template <typename T>
class SinglyLinkedList
{
private:
    /** an integer to count number of nodes in the lest */
    int count = 0;

    /** bool value as a flag to indicate if tail became head */
    bool switchTail = false;

    /** pointers of type Nude */
    Node<T> *head, *tail, *current, *temp, *prev;

public:
    /**
        Base constructor.
       
        If needed, can have other constructors for move and copy.
     */
    SinglyLinkedList();

    /**
        Base destructor.
   
        Calls destructor and deletes list elements.
     */
    ~SinglyLinkedList();

    /**
        Adds element to the head of the list.

	    @tparam T value - new node value to insert.
	    @return void.
    */
    void addFirst(T value);

    /**
        Adds last elements to the list. Has O(1) complexity due to the tail ptr.

	    @tparam T value - new node value to insert.
	    @return void.
    */
    void addLast(T value);

    /**
        Adds new node before specified element in the list.

        @tparam T a new value to insert before a specific node.
        @tparam T value of a node, before which to insert a new value.
	    @return void.
    */
    void addBefore(T value, T node);

    /**
        Adds new node after specified element in the list.

        @tparam T value of a specific node to insert a new data after.
        @tparam T value of a node, after which to insert a new value.
	    @return void.
    */
    void addAfter(T value, T node);
    bool find(T value);

    /**
        Returns Node with specified value.

	    @tparam T value of a node to retrieve from the list.
	    @return pointer to the node containing specified value.
    */
    Node<T> *getNode(T value);

    /**
        Deletes a specified element in the list.

	    @tparam T value of a node to delete from the list.
	    @return void.
    */
    void deleteNode(T value);

    /**
        Deletes first element in the list.

	    @return void.
    */
    void deleteFirst();

    /**
        Deletes last element in the list.

	    @return void.
    */
    void deleteLast();

    /**
        Reverses list, by re-chaining elementsbackwards
        from the tail to head.

	    @return void.
    */
    void reverseList();

    /**
        Finds and prints out duplicates in <value, repeats> format.

	    @return void.
    */
    void findDuplicates();

    /**
        Delets all elements in the list.

	    @return void.
    */
    void clearList();

    /**
        Prints out all elements in the list.

	    @return void.
    */
    void printList();

    /**
        Gets number of elements in the list i.e, length.

	    @return an integer value.
    */
    int getLength();
};

#endif

#include <unordered_map>
#include <iostream>

template <typename T>
SinglyLinkedList<T>::SinglyLinkedList() : head(nullptr), tail(nullptr), current(nullptr),
                                          temp(nullptr), prev(nullptr){};
template <typename T>
SinglyLinkedList<T>::~SinglyLinkedList()
{
    if (this->head)
    {
        this->clearList();
    }
};

template <typename T>
void SinglyLinkedList<T>::addFirst(T value)
{
    this->temp = new Node<T>;
    this->temp->value = value;
    this->temp->next = this->head;

    if (!this->tail)
    {
        this->tail = this->temp;
    }

    this->head = this->temp;
    this->count++;
}

// has O(1) complexity due to the tail ptr
template <typename T>
void SinglyLinkedList<T>::addLast(T value)
{
    this->temp = new Node<T>;
    this->temp->value = value;
    this->temp->next = nullptr;

    if (this->tail)
    {
        this->tail->next = this->temp;
        this->tail = this->temp;
    }

    this->count++;
}

//add last iterative
/*
// iterative addLast has O(N) complexity
void SinglyLinkedList::addLast(int key, int value) //
{
    this->temp = new Node;
    this->current = this->head;

    this->temp->key = key;//(4,4)
    this->temp->value = value;
    this->temp->next = NULL;//(4,4), null

    //point first to new first node
    if (this->current == NULL) {
        this->head = this->temp;
    } else {
        while (this->current->next != NULL) {
            // /[ (4,4) (3,3) (2,2) (1,1) ]
            this->current = this->current->next;
        }

        this->current->next = this->temp;
    }
}
*/

template <typename T>
void SinglyLinkedList<T>::addBefore(T value, T node)
{
    this->temp = new Node<T>;
    this->temp->value = value;
    this->current = this->head;

    if (this->current->value == node)
    {
        this->temp->next = this->current;
        this->head = this->temp;
    }
    else
    {
        while (this->current->next->value != node)
        {
            this->current = this->current->next;
        }

        this->temp->next = this->current->next;
        this->current->next = this->temp;
    }

    count++;
}

template <typename T>
void SinglyLinkedList<T>::addAfter(T value, T node)
{
    this->temp = new Node<T>;
    this->temp->value = value;
    this->current = head;

    if (this->tail->value == node)
    {
        this->tail->next = this->temp;
        this->tail = this->temp;
    }
    else
    {
        while (this->current->value != node)
        {
            this->current = this->current->next;
        }

        this->temp->next = this->current->next;
        this->current->next = this->temp;
    }

    count++;
}

template <typename T>
bool SinglyLinkedList<T>::find(T value)
{
    if (!this->head)
    {
        return false;
    }

    this->current = this->head;

    while (this->current)
    {
        if (this->current->value == value)
        {
            return true;
        }

        this->current = this->current->next;
    }
}

template <typename T>
Node<T> *SinglyLinkedList<T>::getNode(T value)
{
    if (!this->head)
    {
        return nullptr;
    }

    this->current = this->head;

    while (this->current->value != value)
    {
        this->current = this->current->next;
    }

    return this->current;
}

template <typename T>
void SinglyLinkedList<T>::deleteNode(T value)
{
    if (!this->head)
    {
        return;
    }

    this->current = this->head;

    while (this->current->value != value)
    {
        if (!this->current->next)
        {
            return;
        }
        else
        {
            this->temp = this->current;
            this->current = this->current->next;
        }
    }

    if (this->current == this->head)
    {
        this->head = this->head->next;
    }
    else
    {
        this->temp->next = this->current->next;
    }

    this->count--;
}

template <typename T>
void SinglyLinkedList<T>::deleteFirst()
{
    if (!this->head)
    {
        return;
    }

    this->current = this->head;
    this->head = this->head->next;
    delete this->current;
    this->current = nullptr;
    this->count--;
}

template <typename T>
void SinglyLinkedList<T>::deleteLast()
{
    if (!this->head)
    {
        return;
    }

    if (!this->head->next)
    {
        delete this->head;
        this->head = nullptr;
        this->count--;

        return;
    }

    this->current = this->head;

    while (this->current->next->next)
    {
        this->current = this->current->next;
    }

    delete this->current->next;
    this->current->next = nullptr;
    this->count--;
}

template <typename T>
void SinglyLinkedList<T>::reverseList()
{
    if (!this->head)
    {
        return;
    }

    this->current = this->head; // (5, 5), (4, 4), (3, 3), (2, 2), (1, 1)

    while (this->current)
    {                         // (5, 5), (4, 4), (3, 3), (2, 2), (1, 1)
        this->temp = this->current->next; // temp = (4, 4), (3, 3), (2, 2), (1, 1), null
        this->current->next = this->prev; // (4, 4) = NULL, (4, 4) = (5, 5), (5, 5) =  (4, 4), (4, 4) = (3, 3), (3, 3) = (2, 2)
        this->prev = this->current;       // NULL = (5, 5), (4, 4), (3, 3), (2, 2), (1, 1)
        this->current = this->temp;       // (5, 5) = (4, 4), (4, 4) = (3, 3), (3, 3) = (2, 2), (2, 2) = (1, 1), (1, 1) = null

        // set first reversed node as tail
        if (!this->switchTail)
        {
            this->tail = this->prev;
            this->switchTail = true;
        }
    }

    this->head = this->prev;
}

template <typename T>
void SinglyLinkedList<T>::findDuplicates()
{
    if (!this->head)
    {
        return;
    }

    std::unordered_map<int, int> nodes;
    this->current = this->head;

    while (this->current)
    {
        if (nodes.find(this->current->value) == nodes.end())
        {
            nodes[this->current->value] = 1;
        }
        else
        {
            nodes[this->current->value]++;
        }

        this->current = this->current->next;
    }

    for (auto node : nodes)
    {
        if (node.second > 1)
        {
            std::cout << node.first << " : " << node.second << std::endl;
        }
    }
}

template <typename T>
void SinglyLinkedList<T>::clearList()
{
    if (!this->head)
    {
        return;
    }

    while (this->head)
    {
        this->temp = this->head->next;
        delete this->head;
        this->head = this->temp;
        this->count--;
    }

    this->head = nullptr;
}

template <typename T>
void SinglyLinkedList<T>::printList()
{
    this->current = this->head;

    while (this->current)
    {
        std::cout << this->current->value << std::endl;
        this->current = this->current->next;
    }
}

template <typename T>
int SinglyLinkedList<T>::getLength()
{
    return this->count;
}