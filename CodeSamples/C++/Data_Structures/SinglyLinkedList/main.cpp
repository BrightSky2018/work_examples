#include "singly_linked_list.h"

int main()
{
    SinglyLinkedList<int> *list = new SinglyLinkedList<int>();

    list->addFirst(1);
    list->addFirst(2);
    list->addFirst(3);
    list->addFirst(4);
    list->addFirst(5);

    list->addLast(4);
    list->addLast(6);
    list->addLast(8);
    list->addLast(11);

    list->findDuplicates();
    list->reverseList();
    list->addAfter(12, 4);
    list->addBefore(6, 5);

    list->deleteFirst();
    std::cout << "find 3 : " << list->find(3) << std::endl;
    list->deleteNode(3);
    std::cout << "find 3 : " << list->find(3) << std::endl;
    list->deleteLast();
    list->addFirst(3);
    std::cout << "find 3 : " << list->find(3) << std::endl;
    
    Node<int> *node = list->getNode(2);
    std::cout << "value before reassignment : " << node->value << std::endl;
    node->value = 123;

    list->printList();
    list->clearList();
    std::cout << "list length : " << list->getLength() << std::endl;

    return 0;
};