#ifndef QUEUE_ARR_H
#define QUEUE_ARR_H

/**
    @brief C++ implementation of the queue data structure.
    Current implmenetation uses array.
*/
// Queue class definition
template <typename T>
class Queue
{
private:
    /** front and back are arry index numbers for
        curresponding first and last elements.
        arrSize is the stack capacity.
     */
    int front, back, arrSize;

    /** array pointer of T type */
    T *array;

public:
    /**
        Unlike on the linked list based queue, the array based queue 
        capacity has to be specified in advance, either from user 
        input or using compile time #define STACK_SIZE;
        
        Current queue capacity is set by a user and has explicit constructor,
        to avoid assigning values  Queue st = 3;
     */
    explicit Queue(int size);

    /**
        Base destructor.
   
        Calls destructor and deletes queue elements.
     */
    ~Queue();

    /**
        Adds a new element to the queue. FIFO algorithm. 
   
        @tparam T value to be added to the queue.
        @return void.
     */
    void enqueue(T value);

    /**
        Removes the very first element from the top of the queue.
   
        @return void.
     */
    void dequeue();

    /**
        Returns value of the very first element from the top of the queue.
   
        @return T type Node value.
     */
    T pick();

    /**
        Prints out all elements stored in the queue.
   
        @return void.
     */
    void printItems();

    /**
        Returns number of elements stored in the queue.
   
        @return  an integer type value.
     */
    int size();
};

#endif

// Queue class implementation
template <typename T>
Queue<T>::Queue(int size) : arrSize(size), array(new T[size]), front(0), back(0)
{
}

template <typename T>
Queue<T>::~Queue()
{
    delete[] array;
}

template <typename T>
void Queue<T>::enqueue(T value)
{
    if (this->back == this->arrSize)
    {
        return;
    }

    this->array[back] = value;
    this->back++;
}

template <typename T>
void Queue<T>::dequeue()
{
    if (this->front != this->back)
    {
        this->front++;
    }
}

template <typename T>
T Queue<T>::pick()
{
    if (this->front == this->back)
    {
        return 0;
    }

    return this->array[this->front];
}

template <typename T>
int Queue<T>::size()
{
    return this->back - this->front;
}

template <typename T>
void Queue<T>::printItems()
{
    if (this->front == this->back)
    {
        return;
    }

    for (int i = this->front; i < this->back; i++)
    {
        std::cout << this->array[i] << std::endl;
    }
}
