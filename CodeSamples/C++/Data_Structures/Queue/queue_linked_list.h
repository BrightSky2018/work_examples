#ifndef QUEUE_LL_H
#define QUEUE_LL_H

/**
    @brief C++ implementation of the queue data structure.
    Current implmenetation uses linked list.
*/
template <typename T>
struct QNode
{
    T data;
    QNode<T> *next;

    QNode() : next(nullptr), data(0) {}
};

// QueueLL class definition
template <typename T>
class QueueLL
{
private:
    int count;
    QNode<T> *head, *temp, *current;

public:
    QueueLL();
    ~QueueLL();

    void enqueue(T data);
    void dequeue();
    T pick();
    int size();
    void printItems();
};

#endif

// QueueLL class implementation
template <typename T>
QueueLL<T>::QueueLL() : head(nullptr), temp(nullptr), current(nullptr), count(0)
{
}

template <typename T>
QueueLL<T>::~QueueLL()
{
    while (this->head)
    {
        this->temp = this->head->next;
        delete this->head;
        this->head = this->temp;
    }

    head = nullptr;
}

template <typename T>
void QueueLL<T>::enqueue(T data)
{
    // can be done  in O(1) by appending QNodes
    // to the tail as addLast() and keping
    // head pointer to the very first element
    this->temp = new QNode<T>;
    this->temp->data = data;
    this->temp->next = nullptr;

    if (!this->head)
    {
        this->head = this->temp;
        this->count++;

        return;
    }

    this->current = this->head;

    while (this->current)
    {
        if (!this->current->next)
        {
            this->current->next = this->temp;
            this->count++;

            return;
        }

        this->current = this->current->next;
    }
}

template <typename T>
void QueueLL<T>::dequeue()
{
    if (this->head)
    {
        this->temp = this->head;
        this->head = this->temp->next;
        delete this->temp;
        this->temp = nullptr;
        this->count--;
    }
}

template <typename T>
T QueueLL<T>::pick()
{
    if (!this->head)
    {
        return 0;
    }

    return this->head->data;
}

template <typename T>
void QueueLL<T>::printItems()
{
    if (!this->head)
    {
        return;
    }

    this->current = this->head;

    while (current)
    {
        std::cout << this->current->data << std::endl;
        this->current = this->current->next;
    }
}

template <typename T>
int QueueLL<T>::size()
{
    return this->count;
}
