#include <iostream>
#include "queue_array.h"
#include "queue_linked_list.h"

struct Node
{
    int data;
    Node *next = nullptr;

    Node(int data)
    {
        this->data = data;
    }
};

// Queue implementation via Array
// enqueue O(1), dequeue O(1), pik O(1),  print O(N), mem. space O(N)
int main()
{
    //std::unique_ptr<Queue<double>> q =
    // Queue<double> *q = new Queue<double>(4); // allocated on heap
    Queue<double> q(4); // allocated on stack
    q.enqueue(1.2);
    q.enqueue(2.2);
    q.enqueue(3.2);
    q.enqueue(4.2);
    q.enqueue(5.2); // triggs queue is full error

    std::cout << "Size : " << q.size() << std::endl;
    std::cout << "pick : " << q.pick() << std::endl;
    std::cout << "pick : " << q.pick() << std::endl;

    std::cout << "Before dequeue" << std::endl;
    q.printItems();

    while (q.size() != 0)
    {
        q.dequeue();
    }

    std::cout << "pick : " << q.pick() << std::endl;
    std::cout << "Size : " << q.size() << std::endl;

    q.printItems();

    Queue<char> qC(4); // allocated on stack
    qC.enqueue('A');
    qC.enqueue('B');
    qC.enqueue('C');
    qC.enqueue('D');
    qC.enqueue('E'); // triggs queue is full error

    std::cout << "Size : " << qC.size() << std::endl;
    std::cout << "pick : " << qC.pick() << std::endl;
    std::cout << "pick : " << qC.pick() << std::endl;

    std::cout << "Before dequeue" << std::endl;
    qC.printItems();

    while (qC.size() != 0)
    {
        qC.dequeue();
    }

    std::cout << "pick : " << qC.pick() << std::endl;
    std::cout << "Size : " << qC.size() << std::endl;

    qC.printItems();


    // std::unique_ptr<QueueLL<double>> q =
    // QueueLL<double> *q = new QueueLL<double>(4); // allocated on heap
    QueueLL<double> qs; // allocated on stack
    qs.enqueue(1.2);
    qs.enqueue(2.2);
    qs.enqueue(3.2);
    qs.enqueue(4.2);
    qs.enqueue(5.2); // triggs queue is full error

    std::cout << "Size : " << qs.size() << std::endl;
    std::cout << "pick : " << qs.pick() << std::endl;
    std::cout << "pick : " << qs.pick() << std::endl;

    std::cout << "Before dequeue" << std::endl;
    qs.printItems();

    while (qs.size() != 0)
    {
        qs.dequeue();
    }

    //std::cout << "Dequeue : " << q.dequeue() << std::endl;
    std::cout << "pick : " << qs.pick() << std::endl;
    std::cout << "Size : " << qs.size() << std::endl;

    qs.printItems();

    QueueLL<char> qCs; // allocated on stack
    qCs.enqueue('A');
    qCs.enqueue('B');
    qCs.enqueue('C');
    qCs.enqueue('D');
    qCs.enqueue('E'); // triggs queue is full error

    std::cout << "Size : " << qCs.size() << std::endl;
    std::cout << "pick : " << qCs.pick() << std::endl;
    std::cout << "pick : " << qCs.pick() << std::endl;

    std::cout << "Before dequeue" << std::endl;
    qCs.printItems();

    while (qCs.size() != 0)
    {
        qCs.dequeue();
    }

    std::cout << "pick : " << qCs.pick() << std::endl;
    std::cout << "Size : " << qCs.size() << std::endl;

    qCs.printItems();

    return 0;
}