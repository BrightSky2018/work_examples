#include <iostream>
#include <vector>
#include "../Queue/queue_linked_list.h"
#include "../Stack/stack_linked_list.h"

/**
 * @brief adds a new edge to a list of vertices.
 * 
 * @param adjList a list of vertices.
 * @param u  vertex u
 * @param v  vertex v
 * @return void
 */
void addEdge(std::vector<int> adjList[], int u, int v)
{
    adjList[u].push_back(v);
    adjList[v].push_back(u);
}

/**
 * @brief prints graph
 * 
 * @param adjList a list of vertices.
 * @param size number of vertices.
 * @return void
 */
void printGraph(std::vector<int> adjList[], int size)
{
    for (int i = 0; i < size; i++)
    {
        std::cout << i;

        for (int x = 0; x < adjList[i].size(); x++)
        {
            std::cout << ", " << adjList[i][x];
        }

        std::cout << std::endl;
    }
}

/**
 * @brief traverses graph in depth along the edges, before backtracking.
 * 
 * @param adjList a list of vertices.
 * @param visited a list of visited vertices.
 * @param sourceNode the starting, from which to traverse.
 */
void depthFirstSearch(std::vector<int> adjList[], std::vector<bool> visited, int sourceNode)
{
    StackLL<int> stack;
    int v = 0;
    stack.push(sourceNode);

    while (stack.size() > 0)
    {
        v = stack.pick();
        visited[v] = true;
        stack.pop();

        std::cout << v << std::endl;

        for (int i = 0; i < adjList[v].size(); i++)
        {
            if (!visited[adjList[v][i]])
            {
                stack.push(adjList[v][i]);
                visited[adjList[v][i]] = true;
            }
        }
    }
}

/**
 * @brief traverses graph in width (checks all the nearby vertices) before backtracking.
 * 
 * @param adjList a list of vertices.
 * @param visited a list of visited vertices.
 * @param sourceNode the starting, from which to traverse.
 */
void breadthFirstSearch(std::vector<int> adjList[], std::vector<bool> visited, int sourceNode)
{
    QueueLL<int> queue;
    int v = 0;
    queue.enqueue(sourceNode);

    while (queue.size() > 0)
    {
        v = queue.pick();
        visited[v] = true;
        queue.dequeue();

        std::cout << v << std::endl;

        for (int i = 0; i < adjList[v].size(); i++)
        {
            if (!visited[adjList[v][i]])
            {
                queue.enqueue(adjList[v][i]);
                visited[adjList[v][i]] = true;
            }
        }
    }
}

int main()
{
    int vertices = 4; // 4 vertices (0, 1, 2, 3)
    std::vector<bool> visited(vertices, false);
    std::vector<int> adjList[vertices];

    /*
        0 ----- 1
        |  \    |
        |    \  |
        2 ----- 3   
    */

    addEdge(adjList, 0, 1);
    addEdge(adjList, 0, 2);
    addEdge(adjList, 0, 3);
    addEdge(adjList, 2, 3);
    addEdge(adjList, 3, 1);
    addEdge(adjList, 1, 2);


    //printGraph(adjList, 4);

    depthFirstSearch(adjList, visited, 0);
    breadthFirstSearch(adjList, visited, 0);

    return 0;
}