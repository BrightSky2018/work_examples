#include <iostream>
#include "doubly_linked_list.h"

int main()
{
    DoublyLinkedList<int> dll0;
    dll0.addFirst(3);
    dll0.addFirst(4);
    dll0.addFirst(5);
    dll0.addFirst(6);
    dll0.addLast(1);
    dll0.deleteItem(6);
    dll0.printList();
    dll0.addBefore(1, 0);
    dll0.addAfter(1, 2);
    dll0.addAfter(6, 8);
    dll0.addBefore(8, 7);
    dll0.addAfter(2, 3);
    dll0.printList();

    std::cout << "\n ======== Delete first and last elements ======== " << std::endl;

    dll0.deleteFirst();
    dll0.deleteLast();
    dll0.printList();

    std::cout << "\n ======== Print reversed DLL ======== " << std::endl;

    dll0.printReverse();

    std::cout << "\n ======== Get item ======== " << std::endl;

    Node<int> *node = dll0.getItem(3);

    if (node)
    {
        std::cout << node->data << std::endl;
    }

    std::cout << "\n ======== Copy and Move ======== " << std::endl;

    DoublyLinkedList<int> dll1{dll0};
    dll1.printList();

    DoublyLinkedList<int> dll2;
    dll2 = dll0;
    dll2.printList();

    DoublyLinkedList<int> dll3;
    dll3 = std::move(dll0);
    dll3.printList();

    dll1.clearList();
    dll2.clearList();
    dll3.clearList();

    return 0;
}