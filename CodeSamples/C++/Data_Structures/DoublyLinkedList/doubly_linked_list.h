#ifndef DLL_H
#define DLL_H

// DLL Node
template <typename T>
struct Node
{
    /** @tparam T of any value - represents node value */
    T data;

    /** pointers of type Nude */
    Node<T> *next, *prev;

    /**
        @brief  Base constructor.
       
        If needed, can have other constructors for move and copy
     */
    Node() : next(nullptr), prev(nullptr), data(0) {}
};

// DLL class definition

/**
    @brief C++ implementation of doubly linked list data structure.

	@tparam T   Any value float, double, long double, char, string and etc.
 */
template <typename T>
class DoublyLinkedList
{
private:
    /** an integer to count number of nodes in the lest */
    int count;

    /** pointers of type Nude */
    Node<T> *head, *tail, *temp, *newNode, *current;

    /**
        A function to copy an instance of the DoublyLinkedList class.

	    @param obj  reference to doubly linked list one wants to copy.
	    @return     void.
    */
    void deepCopy(const DoublyLinkedList &obj);

    /**
        A function to copy an instance of the DoublyLinkedList class.

	    @param obj  a universal reference to doubly linked list one wants to move.
	    @return     void.
    */
    void moveOwnership(DoublyLinkedList &&obj);

public:
    /**
        Base constructor.
       
        If needed, can have other constructors for move and copy
     */
    DoublyLinkedList();

    /**
        Base destructor.
   
        Calls destructor and deletes list elements.
     */
    ~DoublyLinkedList();

    /**
        Base constructor for copy.
       
        @param obj  reference to doubly linked list one wants to copy.
     */
    DoublyLinkedList(const DoublyLinkedList &obj);

    /**
        Base constructor using assignment operator.
       
        @param obj  reference to doubly linked list one wants to copy.
     */
    DoublyLinkedList &operator=(const DoublyLinkedList &obj);

    /**
        Move constructor.
       
        @param obj  a universal reference to doubly linked list one wants to move.
     */
    DoublyLinkedList(DoublyLinkedList &&obj);

    /**
        Move constructor using assignment operator.
       
        @param obj  a universal reference to doubly linked list one wants to move.
     */
    DoublyLinkedList &operator=(DoublyLinkedList &&obj);

    /**
        Adds element to the head of the list.

	    @tparam T data - new node value to insert in the list.
	    @return void.
    */
    void addFirst(T data);

    /**
        Adds element to the tail of the list.

	    @tparam T data - new node value to insert.
	    @return equation result as a float point type T.
    */
    void addLast(T data);

    /**
        Adds new node before specified element in the list.

        @tparam T node - value of a specific node to insert a new data after.
        @tparam T data - new node value to insert.
	    @return void.
    */
    void addBefore(T node, T data);

    /**
        Adds new node after specified element in the list.

        @tparam T node - value of a specific node to insert a new data after.
        @tparam T data - new node value to insert.
	    @return void.
    */
    void addAfter(T node, T data);

    /**
        Deletes first element in the list.

	    @return void.
    */
    void deleteFirst();

    /**
        Deletes last element in the list.

	    @return void.
    */
    void deleteLast();

    /**
        Deletes first element in the list.

	    @tparam T data - value of a node to delete from the list.
	    @return void.
    */
    void deleteItem(T data);

    /**
        Returns Node with specified value.

	    @tparam T data - value of a node to retrieve.
	    @return pointer to the node containing specified value.
    */
    Node<T> *getItem(T data);

    /**
        Prints list starting from head and towards tail.

	    @return void.
    */
    void printList();

    /**
        Prints list starting from tail and towards head.

	    @return void.
    */
    void printReverse();

    /**
        Removes all elements from the list.

	    @return void.
    */
    void clearList();

    /**
        Method to get length of the list.

	    @return an integer value - the length of the list.
    */
    int getLength();
};

#endif

// DLL class implementation
template <typename T>
void DoublyLinkedList<T>::deepCopy(const DoublyLinkedList &obj)
{
    delete this->head;

    this->count = obj.count;
    this->current = obj.head;

    while (this->current)
    {
        this->temp = new Node<T>;
        this->temp->data = this->current->data;

        if (!this->head)
        {
            this->tail = this->temp;
        }
        else
        {
            this->head->prev = this->temp;
        }

        this->temp->next = this->head;
        this->head = this->temp;

        std::cout << "copying data : " << this->temp->data << std::endl;
        this->current = this->current->next;
        this->count++;
    }

    delete this->current;
    this->current = nullptr;
}

template <typename T>
void DoublyLinkedList<T>::moveOwnership(DoublyLinkedList &&obj)
{
    // Copy the data pointer and its count, switchtail from the
    // source object.
    this->count = obj.count;
    this->head = obj.head;
    this->tail = obj.tail;
    this->current = obj.current;
    this->temp = obj.temp;
    this->newNode = obj.newNode;

    // Release the data pointer from the source object so that
    // the destructor does not free the memory multiple times.
    obj.count = 0;
    obj.head = nullptr;
    obj.tail = nullptr;
    obj.temp = nullptr;
    obj.current = nullptr;
    obj.newNode = nullptr;
}

template <typename T>
DoublyLinkedList<T>::DoublyLinkedList() : head(nullptr), tail(nullptr),
                                          temp(nullptr), newNode(nullptr), count(0)
{
}

// DLL class implementation
template <typename T>
DoublyLinkedList<T>::DoublyLinkedList(const DoublyLinkedList &obj) : head(nullptr), tail(nullptr),
                                                                     temp(nullptr), newNode(nullptr), count(0)
{
    std::cout << "deep copy constructor is called" << std::endl;
    this->deepCopy(obj);
}

// DLL class implementation
template <typename T>
DoublyLinkedList<T> &DoublyLinkedList<T>::operator=(const DoublyLinkedList &obj)
{
    std::cout << "deep copy assignment constructor is called" << std::endl;

    if (&obj == this)
    {
        return *this;
    }

    this->deepCopy(obj);

    return *this;
}

// DLL class implementation
template <typename T>
DoublyLinkedList<T>::DoublyLinkedList(DoublyLinkedList &&obj) : head(nullptr), tail(nullptr),
                                                                temp(nullptr), newNode(nullptr), count(0)
{
    std::cout << "move constructor is called" << std::endl;

    this->moveOwnership(std::move(obj));
}

// DLL class implementation
template <typename T>
DoublyLinkedList<T> &DoublyLinkedList<T>::operator=(DoublyLinkedList &&obj)
{
    std::cout << "move assignment constructor is called" << std::endl;

    if (&obj == this)
    {
        return *this;
    }

    this->moveOwnership(std::move(obj));

    return *this;
}

template <typename T>
DoublyLinkedList<T>::~DoublyLinkedList()
{
    if (this->head)
    {
        this->clearList();
    }
}

template <typename T>
void DoublyLinkedList<T>::addFirst(T data)
{
    this->temp = new Node<T>;
    this->temp->data = data;

    if (!this->head)
    {
        this->tail = this->temp;
    }
    else
    {
        this->head->prev = this->temp;
    }

    this->temp->next = this->head;
    this->head = this->temp;
    this->count++;
}

template <typename T>
void DoublyLinkedList<T>::addLast(T data)
{
    this->temp = new Node<T>;
    this->temp->data = data;

    if (!this->head)
    {
        this->tail = this->temp;
    }
    else
    {
        this->tail->next = this->temp;
        this->temp->prev = this->tail;
    }

    //point last to new last node
    this->tail = this->temp;
    this->count++;
}

template <typename T>
void DoublyLinkedList<T>::addBefore(T node, T data)
{
    if (!this->head)
    {
        return;
    }

    this->newNode = new Node<T>;
    this->newNode->data = data;

    if (this->tail->data == node)
    {
        this->tail->next = this->newNode;
        this->newNode->prev = this->tail;
        this->tail = this->newNode;
        this->count++;
    }
    else
    {
        this->current = this->head;

        while (this->current)
        {
            if (this->current->data == node)
            {
                this->current->next->prev = this->newNode;
                this->newNode->prev = this->current;
                this->newNode->next = this->current->next;
                this->current->next = this->newNode;
                this->count++;

                return;
            }
            else
            {
                this->current = this->current->next;
            }
        }
    }
}

template <typename T>
void DoublyLinkedList<T>::addAfter(T node, T data)
{
    if (!this->head)
    {
        return;
    }

    this->newNode = new Node<T>;
    this->newNode->data = data;

    if (this->head->data == node)
    {
        this->head->prev = this->newNode;
        this->newNode->next = this->head;
        this->head = this->newNode;
        this->count++;
    }
    else
    {
        this->current = this->head;

        while (this->current)
        {
            if (this->current->data == node)
            {
                this->current->prev->next = this->newNode;
                this->newNode->prev = this->current->prev;
                this->newNode->next = this->current;
                this->current->prev = this->newNode;
                this->count++;

                return;
            }
            else
            {
                this->current = this->current->next;
            }
        }
    }
}

template <typename T>
void DoublyLinkedList<T>::deleteFirst()
{
    if (!this->head)
    {
        return;
    }

    this->temp = this->head->next;
    this->temp->prev = this->head->prev;
    delete this->head;
    this->head = this->temp;
    this->count--;
}

template <typename T>
void DoublyLinkedList<T>::deleteLast()
{
    if (!this->tail)
    {
        return;
    }

    this->temp = this->tail->prev;
    this->temp->next = this->tail->next;
    delete this->tail;
    this->tail = this->temp;
    this->count--;
}

template <typename T>
void DoublyLinkedList<T>::deleteItem(T data)
{
    if (!this->head)
    {
        return;
    }

    if (this->head->data == data)
    {
        this->deleteFirst();
    }
    else if (this->tail->data == data)
    {
        this->deleteLast();
    }
    else
    {
        this->current = this->head;

        while (this->current)
        {
            if (this->current->data == data)
            {
                this->current->prev->next = this->current->next;
                this->current->next->prev = this->current->prev;
                delete this->current;
                this->current = nullptr;
                this->count--;

                return;
            }
            else
            {
                this->current = this->current->next;
            }
        }
    }
}

template <typename T>
void DoublyLinkedList<T>::printList()
{
    if (!this->head)
    {
        return;
    }

    this->current = this->head;

    while (this->current)
    {
        std::cout << "data : " << this->current->data << " tail : " << this->tail->data << std::endl;
        this->current = this->current->next;
    }
}

template <typename T>
void DoublyLinkedList<T>::printReverse()
{
    if (!this->tail)
    {
        return;
    }

    this->current = this->tail;

    while (this->current)
    {
        std::cout << "data : " << this->current->data << std::endl;
        this->current = this->current->prev;
    }
}

template <typename T>
Node<T> *DoublyLinkedList<T>::getItem(T data)
{
    if (!this->head)
    {
        return nullptr;
    }

    this->current = this->head;

    while (this->current)
    {
        if (this->current->data == data)
        {
            return this->current;
        }

        this->current = this->current->next;
    }

    return this->current;
}

template <typename T>
int DoublyLinkedList<T>::getLength()
{
    return this->count;
}

template <typename T>
void DoublyLinkedList<T>::clearList()
{
    if (!this->head)
    {
        return;
    }

    while (this->head)
    {
        this->temp = this->head->next;
        delete this->head;
        this->head = this->temp;
        this->count--;
    }

    this->head = nullptr;
}