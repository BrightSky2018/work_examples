
#include <iostream>
#include <vector>

#ifndef MAN_HEAP_H
#define MAN_HEAP_H

/**
 * @brief C++ implementation of max heap data structure
 * 
 */
class MaxHeap
{
private:
    /** properties of the calss to count and store heap elements */
    int count{0};
    std::vector<int> heap;

public:
    /**
 * @brief Construct a new Min Heap object
 * 
 * @param size 
 */
    MaxHeap(int size);

    /**
     * @brief Destroy the Min Heap object
     * 
     */
    ~MaxHeap();

    /**
     * @brief swaps elements
     * 
     * @param x 
     * @param y
     * @return void
     */
    void swap(int &x, int &y);

    /**
     * @brief adds new element
     * 
     * @param data
     * @return void
     */
    void add(int data);

    /**
     * @brief prints heap
     * 
     * @return void
     */
    void printHeap();
};

#endif


MaxHeap::MaxHeap(int size = 0)
{
    this->heap.resize(size);
}

MaxHeap::~MaxHeap()
{
    // code to destroy the heap storage.
    // in case with vector not needed, since - https://en.cppreference.com/w/cpp/container/vector/~vector
    std::cout << "heap destroyed" << std::endl;
}

void MaxHeap::swap(int &x, int &y)
{
    int temp{x};
    x = y;
    y = temp;
}

void MaxHeap::add(int data)
{
    // Arr[(i - 1) / 2] Returns the parent node
    // Arr[(2 * i) + 1] Returns the left child node
    // Arr[(2 * i) + 2] Returns the right child node

    //11, 50, 19, 20, 12
    this->heap[this->count] = data; // 11
    this->count++;                  // 1

    int i{this->count - 1};

    // heapify max.
    // heapify can easily become MIN if to cahnge > to <
    while (i > 0 && this->heap[i] > this->heap[(i - 1) / 2])
    {
        swap(this->heap[i], this->heap[(i - 1) / 2]);
        i = (i - 1) / 2;
    }
}

void MaxHeap::printHeap()
{
    for (auto &item : this->heap)
    {
        std::cout << "item " << item << std::endl;
    }
}
