#include "max_heap.h"

int main()
{
    // 11, 50, 19, 20, 12
    //    11
    //   /
    // 50
    MaxHeap hp{5};
    hp.add(11);
    hp.add(50);
    hp.add(19);
    hp.add(20);
    hp.add(12);

    hp.printHeap();

    return 0;
}