#!/bin/bash

# The below code is just an example to show ability to work with bash
# Can work with - files using sed/awk/find, processes, monitor
# system status, write installation scripts, manage database, SSH and etc...
# Exactly the same can do using PowerShell. Understand architecture of the
# Shell, its core elements and purpose of being on top of the Kernel.

# Evaluate commands silently
function writeCmd() {
    if [[ ${_q} -eq 0 ]]; then
        eval $@
    else
        eval $@ > /dev/null
    fi
}

var=`hostnamectl | grep 'hostname'`
echo $var
# Menu header and options
echo "Please enter your choice: "
options=("Check Postgres" "Install Postgres" "Create Database" "Delete Database" "Quit")

select opt in "${options[@]}"
do
    case $opt in
        "Check Postgres")
            # Option can be extended with various commands and condition checks.
            echo "Getting Postgres status..."
            writeCmd "apt list --installed | grep postgresql"
            ;;
        "Install Postgres")
            # Could have check on version compatabilites etc.
            echo "Installing postgres..."
            writeCmd "apt-get update"
            writeCmd "apt-get install postgresql postgresql-contrib"
            writeCmd "sudo service postgresql restart"
            ;;
        "Create Database")
            echo "Enter database name to be created"
            read DBNAME
            echo "Create database ? (Y/N):"
            read IN

            if [ "$IN" = "y" -o "$IN" = "Y" ]; then
                writeCmd "su postgres" <<EOF
psql -c "CREATE DATABASE $DBNAME;"
EOF
            fi
            ;;
        "Delete Database")
            echo "Enter database name to be deleted"
            read DBNAME
            echo "Delete database ? (Y/N):"
            read IN

            if [ "$IN" = "y" -o "$IN" = "Y" ]; then
                writeCmd "su postgres" <<EOF
psql -c "DROP DATABASE $DBNAME;"
EOF
            fi
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option";;
    esac
done
