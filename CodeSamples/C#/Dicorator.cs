using System;

namespace DesignPatterns
{
    // Used to enforce curtain rules for implementing classes
    interface IVehicle
    {
        void SetType(string type);
        void SetProducer(string producer);
        string ShowTechincalDetails();
    }

    // Can be a Car, a Plane and anything which reprents Vehicles
    class Car : IVehicle
    {
        private string model;
        private string producer;

        public Car(string model = null, string producer = null)
        {
            this.model = model;
            this.producer = producer;
        }

        public void SetType(string model)
        {
            this.model = model;
        }

        public void SetProducer(string producer)
        {
            this.producer = producer;
        }

        public virtual string ShowTechincalDetails()
        {
            return "Model: " + this.model + " Producer: " + this.producer;
        }
    }

    // Extends Car or can directly extend Interface, depends 
    // on - if we need interface methods inside a decorator
    class CarEngine : Car
    {
        readonly Car car;
        private string engine;

        public CarEngine(Car car)
        {
            this.car = car;
        }

        public void SetEngine(string engine)
        {
            this.engine = engine;
        }

        public override string ShowTechincalDetails()
        {
            return car.ShowTechincalDetails() + " Engine Type: " + this.engine;
        }
    }

    class Client
    {
        public static void Main()
        {
            Car car = new Car("BMW X5", "China");
            Console.WriteLine(car.ShowTechincalDetails());

            // Structural design patter that allows to modify an Object without 
            // directly extending/modifying its functionality. Decorator serves as a wrapper 
            // around an Object and extends its with desired functionality or may alter(override) it
            CarEngine decorator = new CarEngine(car);
            decorator.SetEngine("Turbo Disel");
            Console.WriteLine(decorator.ShowTechincalDetails());

            Console.Read();
        }
    }
}
