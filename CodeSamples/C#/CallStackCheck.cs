using System;
using System.Diagnostics;
using System.Reflection;

namespace AlgorithmsAndDatastructures
{
    class CallStack
    {
        public int Fibonacci(int n)
        {
            if (n <= 1)
            {
                return n;
            }

            this.GetStackFrames();

            return Fibonacci(n - 1) + Fibonacci(n - 2);
        }

        public void GetStackFrames()
        {
            StackTrace trace = new StackTrace();

            foreach (StackFrame frame in trace.GetFrames())
            {
                // We can use other MethodBase fucntion do extract 
                // additional information from the call stack
                MethodBase method = frame.GetMethod(); 
                Console.WriteLine(method);
            }
        }

        public static void Main()
        {
            CallStack cs = new CallStack();
            cs.Fibonacci(4);

            Console.ReadKey();
        }
    }
}
