﻿using System;

namespace DynamicProgramming
{

    class Factorial
    {
        // top down 
        public int FactorialTopDown(int n)// 4
        {
            int result = 1;

            if (n < 1)
            {
                return 1;
            }

            result = n * FactorialTopDown(n - 1);//  4
                                                  //4 * 6
                                                  //  3 * 2
                                                  //    2 * 1
                                                  //      1 * 0
            return result;
        }

        // bottom up
        public int FactorialBottomUp(int n)//4
        {
            int result = 1;

            if (n < 1)
            {
                return 1;
            }

            for (int num = 1; num <= n; num++)// 1 * 1, 2 * 1, 3 * 2, 6 * 4 = 1, 2, 6, 24
            {
                result *= num;
            }

            return result;
        }
    }

    // recursion
    class FibonacciRecusion
    {
        public int Fibonacci(int n)
        {
            if (n <= 1)
            {
                return n;
            }

            return Fibonacci(n - 1) + Fibonacci(n - 2);
        }
    }

    // top down
    class FibonacciMemoization
    {
        private int[] lookup = new int[30];

        public int Fibonacci(int n)
        {
            if (lookup[n] == 0)// if number is in the look up return it, else compute
            {
                if (n <= 1)
                {
                    lookup[n] = n;
                }                   
                else
                {
                    lookup[n] = Fibonacci(n - 1) + Fibonacci(n - 2); 
                }        
            }

            return lookup[n];
        }

        public void LookUp()
        {
            for (int i = 0; i < lookup.Length; i++)
            {
                Console.WriteLine("Fibonacci number is {0}", lookup[i]);
            }
        }
    }

    // bottom up
    class FibonacciTabulation
    {
        public int Fibonacci(int n)
        {
            int[] f = new int[n + 1];
            f[0] = 0;
            f[1] = 1;

            for (int i = 2; i <= n; i++)
            {
                f[i] = f[i - 1] + f[i - 2];
            }
               
            return f[n];
        }
    }

    class Program
    {
        public static void Main()
        {
            Factorial factorial = new Factorial();
            FibonacciMemoization fib1 = new FibonacciMemoization();
            FibonacciTabulation fib2 = new FibonacciTabulation();
            
            Console.WriteLine("\n ----------- Factorial TopDown ----------- \n");
            Console.WriteLine(factorial.FactorialTopDown(6));
            Console.WriteLine("\n ----------- Factorial BottomUp ----------- \n");
            Console.WriteLine(factorial.FactorialBottomUp(6));
           
            fib1.SetLookupTable();
            Console.WriteLine("\n ----------- Fibonacci TopDown ----------- \n");
            Console.WriteLine(fib1.Fibonacci(20));
            fib1.LookUp();
            Console.WriteLine("\n ----------- Fibonacci BottomUp ----------- \n");
            Console.WriteLine(fib2.Fibonacci(20));
            Console.ReadKey();
        }
    }
}
