using System;
using System.Collections.Generic;

namespace DesignPatterns
{
    // Can be extended by several factories, which produce a specific 
    // set of components, however belong to the Device category. For 
    // instance ControlDeviceFactory is responsible for production of I/O 
    // devices, while SoundDeviceFactory can be focused on sound devices.
    abstract class DeviceFactory
    {
        public abstract void CreateDevice(string name, string type, string producer);
        public abstract DeviceComponent GetDevice(string name);
    }

    // ControlDevices as well as Sound Devices can extend
    // this abstract class
    abstract class DeviceComponent
    {
        protected string type;
        protected string producer;

        public abstract string ControlDeviceInfo();
    }

    class KeyBoard : DeviceComponent
    {
        public KeyBoard(string type, string producer)
        {
            this.type = type;
            this.producer = producer;
        }

        public override string ControlDeviceInfo()
        {
            return "Type: " + this.type + " Producer: " + this.producer;
        }
    }

    class Mouse : DeviceComponent
    {
        public Mouse(string type, string producer)
        {
            this.type = type;
            this.producer = producer;
        }

        public override string ControlDeviceInfo()
        {
            return "Type: " + this.type + " Producer: " + this.producer;
        }
    }

    class HeadPhones : DeviceComponent
    {
        public HeadPhones(string type, string producer)
        {
            this.type = type;
            this.producer = producer;
        }

        public override string ControlDeviceInfo()
        {
            return "Type: " + this.type + " Producer: " + this.producer;
        }
    }

    // Simplified factory to create SoundDeviceComponent Objects
    class SoundDeviceFactory : DeviceFactory
    {
        private DeviceComponent device;

        public override void CreateDevice(string name, string type, string producer)
        {
            this.device = new HeadPhones(type, producer);
        }

        public override DeviceComponent GetDevice(string name)
        {
            return this.device;
        }
    }

    // Factory to create ControlDeviceComponent Objects
    class ControlDeviceFactory : DeviceFactory
    {
        private readonly Dictionary<string, DeviceComponent> devices = new Dictionary<string, DeviceComponent>();
        private DeviceComponent device;

        public override void CreateDevice(string name, string type, string producer)
        {
            if (this.devices.ContainsKey(name))
            {
                Console.WriteLine("Device has been already crated !");
                return;
            }
            else
            {
                switch (name)
                {
                    case "KeyBoard":
                        this.device = new KeyBoard(type, producer);
                        break;
                    case "Mouse":
                        this.device = new Mouse(type, producer);
                        break;
                }

                this.devices.Add(name, this.device);
            }
        }

        public override DeviceComponent GetDevice(string name)
        {
            if (this.devices.ContainsKey(name))
            {
                this.device = this.devices[name];
            }

            return this.device;
        }
    }

    class Client
    {
        public static void Main(string[] args)
        {
            ControlDeviceFactory deviceFactory = new ControlDeviceFactory();
            SoundDeviceFactory soundFactory = new SoundDeviceFactory();
            string device = null;
            char input;

            do
            {
                Console.WriteLine("There are two control device available to be crated");
                Console.WriteLine(" Press K - to create Keybord \n Press M - to craete Mouse" +
                    " \n Press L - to Get Device \n Press X - to Exit");
                input = Console.ReadKey().KeyChar;

                if (input.Equals('K'))
                {
                    device = "KeyBoard";

                    Console.WriteLine("\nEnter type of {0}", device);
                    string type = Console.ReadLine();
                    Console.WriteLine("\nEnter producer of {0}", device);
                    string producer = Console.ReadLine();

                    deviceFactory.CreateDevice(device, type, producer);
                }
                else if(input.Equals('M'))
                {
                    device = "Mouse";

                    Console.WriteLine("\nEnter type of {0}", device);
                    string type = Console.ReadLine();
                    Console.WriteLine("\nEnter producer of {0}", device);
                    string producer = Console.ReadLine();

                    deviceFactory.CreateDevice(device, type, producer);
                }
                else if(input.Equals('L'))
                {
                    do
                    {
                        Console.WriteLine("\n Press K - to Get Keybord \n Press M - to Get Mouse \n Press V - Exit");
                        input = Console.ReadKey().KeyChar;

                        if (input.Equals('K'))
                        {
                            device = "KeyBoard";
                        }
                        else if (input.Equals('M'))
                        {
                            device = "Mouse";
                        }

                        soundFactory.CreateDevice("HeadPhones", "Stereo", "Razor");
                        Console.WriteLine("\n {0}", deviceFactory.GetDevice(device).ControlDeviceInfo());
                        Console.WriteLine("\n Bonus Sound device - {0}", soundFactory.GetDevice(device).ControlDeviceInfo());
                    } while (!input.Equals('V'));
                }
            } while (!input.Equals('X'));
        }
    }
}
