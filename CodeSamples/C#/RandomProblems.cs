using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodingProblems
{
    class RandomProblems
    {
        // Complete the oddNumbers function below.
        public List<int> OddNumbers(int l, int r)
        {
            List<int> nums = new List<int>();

            for (int i = l; i <= r; i++)
            {
                if ((i % 2) == 1)
                {
                    nums.Add(i);
                }
            }

            return nums;
        }

        //returns the sum of all the multiples of 3 or 5, below the number passed in.
        public int SumOfMultiples(int value)
        {
            int total = 0;

            for (int i = 3; i < value; i++)
            {
                if ((i % 3 == 0) || (i % 5) == 0)
                {
                    total += i;
                }
            }

            return total;
        }

        /* Your task is to make a function that can take any 
         * non-negative integer as a argument and return it with its digits in descending order.
         * Essentially, rearrange the digits to create the highest possible number.
         * Examples:
         * Input: 21445 Output: 54421
         */
        public int DescendingOrder(int num)
        {
            int result = 0;

            if (num < 9)
            {
                result = num;
            }

            var numbers = num.ToString().Select(t => int.Parse(t.ToString())).ToArray().OrderByDescending(i => i);
            Int32.TryParse(string.Join("", numbers), out result);

            return result;
        }

        /*
         * Given two integers a and b, which can be positive or negative,
         * find the sum of all the numbers between including
         * them too and return it.If the two numbers are equal return a or b.
         */
        public int GetSum(int a, int b)
        {
            if (a == b)
            {
                return a;
            }

            int max = Math.Max(a, b);
            int min = Math.Min(a, b);

            return (max - min + 1) * (min + max) / 2;
        }

        /* You are going to be given a word. Your job is to return the 
         * character of the word. If the word's length is odd, return
         * the middle character. If the word's length is even, return
         * the middle 2 characters.
         */
        public string GetMiddle(string s)
        {
            //Code goes here!
            if (string.IsNullOrEmpty(s) || s.Length == 0)
            {
                return null;
            }

            string mid = null;

            if (s.Length % 2 != 0)
            {
                mid = s[(0 + s.Length) / 2].ToString();
            }
            else
            {
                mid = s[(-1 + s.Length) / 2] + s[(1 + s.Length) / 2].ToString();
            }

            return mid;
        }

        public string CreatePhoneNumber(int[] numbers)
        {
            if (Array.Exists(numbers, x => x < 0 || x > 9) || numbers.Length > 10)
            {
                return "negative" + numbers.Length;
            }

            return string.Format("({0}{1}{2}) {3}{4}{5}-{6}{7}{8}{9}", Array.ConvertAll(numbers, x => x.ToString()));
        }

        // Find unique value in array
        // can be done via looping and comparing or
        // using LINQ
        public int GetUnique(IEnumerable<int> numbers)
        {
            int unique = numbers.GroupBy(x => x)
                  .Where(g => g.Count() == 1)
                  .Select(y => y.Key)
                  .ToList().FirstOrDefault();

            return unique;
        }

        // Complete the solution so that it splits the string
        // into pairs of two characters. If the string contains
        // an odd number of characters then it should replace 
        // the missing second character of the final pair with an underscore ('_').
        public string[] Solution(string str)
        {
            char[] toTrim = new char[] { ' ' };
            string trimmedStr = str.Trim(toTrim);
            string[] result = new string[trimmedStr.Length];

            if (trimmedStr.Length % 2 == 1)
            {
                trimmedStr += "_";
            }

            for (int i = 0; i < trimmedStr.Length; i += 2)
            {
                result[i / 2] = trimmedStr.Substring(i, 2);
            }

            return result;

            // another solution
            //return Enumerable.Range(0, str.Length)
            //       .Where(x => x % 2 == 0)
            //       .Select(x => str.Substring(x, 2))
            //       .ToArray();
        }

        public char GetChar(char[] chars)
        {
            for (int i = 0; i < chars.Length; i++)
            {
                if (((int)chars[i + 1] - (int)chars[i]) > 1)
                {
                    return (char)((int)chars[i] + 1);
                }
            }

            return ' ';
        }
    }

    class Client
    {
        public static void Main()
        {
            RandomProblems problems = new RandomProblems();

            Console.WriteLine("Odd Numbers");

            foreach (var num in problems.OddNumbers(2, 12))
            {
                Console.WriteLine(num);
            }

            Console.WriteLine("Sum of multiple: {0}", problems.SumOfMultiples(15).ToString());
            Console.WriteLine("Sum of multiple: {0}", problems.SumOfMultiples(15).ToString());
            Console.WriteLine("Descending order: {0}", problems.DescendingOrder(63234).ToString());
            Console.WriteLine("Get sum: {0}", problems.GetSum(-4, 12).ToString());
            Console.WriteLine("Get string middle: {0} {1}", problems.GetMiddle("Test"), problems.GetMiddle("Tesst"));

            int[] arr = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
            Console.WriteLine("Format phone number: {0}", problems.CreatePhoneNumber(arr));

            int[] arr2 = new[] { 11, 11, 14, 11, 11 };
            Console.WriteLine("Get unique element: {0}", problems.GetUnique(arr2));

            string str = "abcdefg    ";
            Console.WriteLine("Get strings: {0}", string.Join("", problems.Solution(str)));

            char[] chars = new[] { 'a', 'b', 'c', 'd', 'f' };
            Console.WriteLine("Get missing char: {0}", problems.GetChar(chars));

            Console.ReadLine();
        }
    }
}