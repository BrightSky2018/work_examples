﻿using System;

namespace GraphAlgorithms
{
    class DijkstrasAlgorithm
    {
        private readonly string[] Vertices = new string[] { " A ", " B ", " C ", " D ", " E ", " F " };

        public void PrintGraph(int[][] data)
        {
            if (data != null)
            {
                Console.WriteLine("   A  B  C  D  E  F ");

                for (int i = 0; i < data.Length; i++)
                {
                    Console.WriteLine(Vertices[i] + data[i][0] + "  " + data[i][1] + "  " + data[i][2] +
                       "  " + data[i][3] + "  " + data[i][4] + "  " + data[i][5] + "\n");
                }
            }
        }

        public void PrintResults(int[] dist, int n)
        {
            Console.Write("Vertices   Shortest paths from the initial Vertex \n");

            for (int i = 0; i < Vertices.Length; i++)
            {
                Console.Write(Vertices[i] + " \t\t " + dist[i] + "\n");
            }
        }

        public int GetClosestVertex(int[] verticesDist, bool[] visitedVertices)
        {
            int min = int.MaxValue, vertexIndex = -1;

            for (int i = 0; i < Vertices.Length; i++)
            {
                if (visitedVertices[i] == false && verticesDist[i] <= min)
                {
                    min = verticesDist[i];
                    vertexIndex = i;
                }

            }

            return vertexIndex;
        }

        public void FindShortestPaths(int[][] graph, int src)
        {
            // array of vertices. each vertice represented by
            // array index and distance from the source vertice
            // under each index
            int[] verticesDist = new int[Vertices.Length];

            // array of visited vertices
            bool[] visitedVertices = new bool[Vertices.Length];

            for (int i = 0; i < Vertices.Length; i++)
            {                                   //        0A   1B   2C   3E   4R   5F
                verticesDist[i] = int.MaxValue; // dist[]{max, max, max, max, max, max}
                visitedVertices[i] = false; // visited[]{false, false, false, false, false, false}
            }
             
            // distance to a source vertice
            verticesDist[src] = 0; // dist[]{ 0, max, max, max, max, max}
          
            // iterate over rows of a graph
            for (int i = 0; i < Vertices.Length - 1; i++)
            {
                // index of closest vertice to visit, while traversing a graph 
                int vertexIndex = GetClosestVertex(verticesDist, visitedVertices);
                                                    
                visitedVertices[vertexIndex] = true;
                                                                                                                                      
                for (int v = 0; v < Vertices.Length; v++)
                {
                    if (!visitedVertices[v] && graph[vertexIndex][v] != 0 && verticesDist[vertexIndex]
                        != int.MaxValue && verticesDist[vertexIndex] + graph[vertexIndex][v] < verticesDist[v])
                    {
                        verticesDist[v] = verticesDist[vertexIndex] + graph[vertexIndex][v];
                    }
                }

                Console.WriteLine(string.Join(", ", verticesDist));
            }

            PrintResults(verticesDist, Vertices.Length);
        }

        public static void Main()
        {
            // The overall runtime complexity is O(V^2) since 
            // AdjacencyMatrix (arr[][]) is used, however can be O(ELogV)
            // if AdjacencyList is implemented
            // Good to use if we want to find all shortest paths to all verteces
            // from a start vertex
            int[][] graph = new int[][]
            {                  /* A  B  C  D  E  F */
                /* A */new int[]{ 0, 3, 5, 12, 0, 0 },   //B---7---D---4---E
                /* B */new int[]{ 3, 0, 0, 7, 0, 0 },    //|     / |       |
                /* C */new int[]{ 5, 0, 0, 6, 0, 8 },    //3   12  6       2
                /* D */new int[]{ 12, 7, 6, 0, 4, 0 },   //| /     |       |
                /* E */new int[]{ 0, 0, 0, 4, 0, 2 },    //A---5---C---8---F
                /* F */new int[]{ 0, 0, 8, 0, 2, 0 }
            };

            DijkstrasAlgorithm da = new DijkstrasAlgorithm();
            da.PrintGraph(graph);
            da.FindShortestPaths(graph, 0);

            Console.ReadKey();
        }
    }
}
