using System;

namespace DesignPatterns
{

    // Book Interface, could be an abstract class
    interface IBook
    {
        void SetTitle(string title);
        void SetAuthor(string author);
        void SetPublisher(string publisher);
        void SetPrice(int price);
    }

    // Class book
    class Book : IBook
    {
        private string title;
        private string author;
        private string publisher;
        private int price;

        public void SetTitle(string title)
        {
            this.title = title;
        }

        public void SetAuthor(string author)
        {
            this.author = author;
        }

        public void SetPublisher(string publisher)
        {
            this.publisher = publisher;
        }

        public void SetPrice(int price)
        {
            this.price = price;
        }

        public string PrintBook()
        {
            return "Title: " + title + " Author: " + author + " Publisher: " + publisher + " Price: " + price;
        }
    }

    // Proxy class to create and get control over the Book class
    class BookProxy 
    {
        Book book;

        public void InitializeBook(string title, string author, string publisher, int price)
        {
            if (book == null)
            {
                book = new Book();
            }

            book.SetTitle(title);
            book.SetAuthor(author);
            book.SetPublisher(publisher);
            book.SetPrice(price);
        }

        public string GetBook()
        {
            if (book == null)
            {
                book = new Book();
            }

            return book.PrintBook();
        }
    }

    // The below proxy allows to control access to an Object
    class AccessControlProxy
    {
        Book book;
        private readonly string password = "abc";

        public string GainAccess(string password)
        {
            if (this.password != password)
            {
                return "Invalid creadentials !";
            }

            book = new Book();

            return "A new book has been Initialize !";
        }

        public string SetBook(string title, string author, string publisher, int price)
        {
            if (book == null)
            {
                return "Only authorized users can access the object !";
            }
           
            book.SetTitle(title);
            book.SetAuthor(author);
            book.SetPublisher(publisher);
            book.SetPrice(price);

            return "A new book has been set up !";
        }

        public string GetBook()
        {
            if (book == null)
            {
                return "Only authorized users can access the object !";
            }

            return book.PrintBook();
        }
    }

    class Client
    {
        public static void Main()
        {
            BookProxy book = new BookProxy();
            book.InitializeBook("Test title", "Test author", "Test publisher", 0);
            Console.WriteLine(book.GetBook() + "\n");

            AccessControlProxy book2 = new AccessControlProxy();
            Console.WriteLine(book2.GainAccess("qwe") + "\n");
            Console.WriteLine(book2.SetBook("Test title", "Test author", "Test publisher", 0) + "\n");
            Console.WriteLine(book2.GetBook() + "\n");
            Console.WriteLine(book2.GainAccess("abc") + "\n");
            Console.WriteLine(book2.SetBook("Test title", "Test author", "Test publisher", 0) + "\n");
            Console.WriteLine(book2.GetBook() + "\n");

            Console.ReadKey();
        }
    }
}
