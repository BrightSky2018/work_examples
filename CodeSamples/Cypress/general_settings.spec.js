/// <reference types="Cypress" />

context('First Test', () => {
    beforeEach(() => {
        cy.visit ('http://localhost:3000/');   
    })

    it('Test Locales', () => {
        const languageOptionsArray = [
            {
              key: 'en_CA',
              value: 'English (Canada)',
            },
            {
              key: 'en_GB',
              value: 'English (Britain)',
            },
            {
              key: 'en_US',
              value: 'English (USA)',
            },
            {
              key: 'it_IT',
              value: 'Italiano',
            },
            {
              key: 'pl_PL',
              value: 'Polski',
            },
            {
              key: 'pt_BR',
              value: 'Português (Brasil)',
            },
            {
              key: 'ru_RU',
              value: 'Русский',
            }
        ];

        cy.get('#locale option').then(options => { 
          const locale = [...options].map(o => o.value)
          expect(locale).to.deep.eq(languageOptionsArray.map(x => x.key))
        })

        cy.get('#locale option').then(options => { 
            const locale = [...options].map(o => o.text)
            expect(locale).to.deep.eq(languageOptionsArray.map(x => x.value))
        })
    })

    it('Test Timezones', () => {
        const timezoneOptionsArray = [
            {
              key: '',
              value: 'Use station default',
            },
            {
              key: 'America/Lima',
              value: 'America/Lima',
            },
            {
              key: 'Asia/Yerevan',
              value: 'Asia/Yerevan',
            },
            {
              key: 'Europe/Athens',
              value: 'Europe/Athens',
            },
            {
              key: 'Europe/Belgrade',
              value: 'Europe/Belgrade',
            },
            {
              key: 'Europe/Berlin',
              value: 'Europe/Berlin',
            },
            {
              key: 'Europe/Prague',
              value: 'Europe/Prague',
            },
            {
              key: 'UTC',
              value: 'UTC',
            }
        ];

        cy.get('#timezone option').then(options => { 
            const timeZone = [...options].map(o => o.value)
            expect(timeZone).to.deep.eq(timezoneOptionsArray.map(x => x.key))
        })
  
        cy.get('#timezone option').then(options => { 
            const timeZone = [...options].map(o => o.text)
            expect(timeZone).to.deep.eq(timezoneOptionsArray.map(x => x.value))
        })
    })

    it('Test Week Days', () => {
        const weekDaysArray = [
            {
              key: '0',
              value: 'Sunday',
            },
            {
              key: '1',
              value: 'Monday',
            },
            {
              key: '2',
              value: 'Tuesday',
            },
            {
              key: '3',
              value: 'Wednesday',
            },
            {
              key: '4',
              value: 'Thursday',
            },
            {
              key: '5',
              value: 'Friday',
            },
            {
              key: '6',
              value: 'Saturday',
            }
        ];
      
        cy.get('#weekStartDay option').then(options => { 
            const weekStartDay = [...options].map(o => o.value)
            expect(weekStartDay).to.deep.eq(weekDaysArray.map(x => x.key))
        })
    
        cy.get('#weekStartDay option').then(options => { 
            const weekStartDay = [...options].map(o => o.text)
            expect(weekStartDay).to.deep.eq(weekDaysArray.map(x => x.value))
        })
    })

    it('Test Change Input and Submit', () => {  
      cy.get('#locale').select('English (USA)')
      cy.get('#timezone').select('Europe/Athens')
      cy.get('#weekStartDay').select('Saturday')
      cy.get('#stationDefaultFadeIn').clear().type(11)
      cy.get('#stationDefaultFadeOut').clear().type(12)
      cy.get('#defaultCrossfadeDuration').clear().type(13)
      cy.get('input[type=radio]').first().check()

      cy.fixture('user').then((user) => {  
        cy.get('#spinitron_station').type(user.station);
        cy.get('#spinitron_email').type(user.email);
        cy.get('#spinitron_password').type(user.password);

        cy.get('#spinitron_station').should('have.value', user.station);
        cy.get('#spinitron_email').should('have.value', user.email);
        cy.get('#spinitron_password').should('have.value', user.password);
      })

      cy.get('#submit').click();
      cy.get('pre').contains('Cannot POST /').then(() => {
          console.log('Test has been successful')
      })
    }) 
})
