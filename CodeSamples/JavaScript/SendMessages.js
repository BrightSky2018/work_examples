var formHandler = (function() {

    var displayError = function(element, message) {
        element.style.display = 'block';
        element.innerHTML = message;

        setTimeout(function(){
            element.style.display = 'none';
        }, 2000);
    }
    
    var sendMessage = function() {
        const langSwitch = document.getElementById('language').value;
        const emailRegex = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        let sendMessage = 'send_message';
        let userEmail = document.getElementById('user_email').value;
        let chatMessage = document.getElementById('chat_message').value;
        let error = document.getElementById('error_msg');
        let chat = document.getElementById('chat'); 

        let emptyEmailMsg = 'Введите адрес почты !';
        let invalidEmailMsg = 'Неверный формат почты !';
        let emptyChatMsg = 'Введите ваше сообщение !';

        if (langSwitch == "en") { 
            emptyEmailMsg = 'Please enter your email !';
            invalidEmailMsg = 'Invalid email !';
            emptyChatMsg = 'Please enter your message !';
        }

        if (!userEmail) {
            displayError(error, emptyEmailMsg);
            return;
        } else if (!userEmail.match(emailRegex)) {
            displayError(error, invalidEmailMsg);
            return;
        }
    
        if (!chatMessage) {
            displayError(error, emptyChatMsg);
            return;
        }
 
        const formDataString = {'send_message': sendMessage, 'user_email': userEmail, 'chat_message': chatMessage};
        const url = 'application/models/MessageHandler.php';
        sendFormData('POST', url, formDataString);

        document.getElementById('user_email').value = '';
        document.getElementById('chat_message').value = '';
        chat.style.display = 'none';

        return;
    };

    var sendComment = function() {
        const langSwitch = document.getElementById('language').value;
        let sendComment = 'send_comment';
        let userName = document.getElementById('comment_name').value;
        let commentMessage = document.getElementById('comment_textarea').value;
        let error = document.getElementById('error_msg');

        if (userName && commentMessage) {
            const formDataString = {'send_comment': sendComment, 'comment_name': userName, 'comment_textarea': commentMessage};
            const url = 'application/models/MessageHandler.php';
            sendFormData('POST', url, formDataString);
        }
        
        document.getElementById('comment_name').value = '';
        document.getElementById('comment_textarea').value = '';

        return;
    };

    var sendFormData = function(type, url, formDataString) {
        const langSwitch = document.getElementById('language').value;
        
        $.ajax({
            type: type,
            dataType : "json",
            url: url,
            data: formDataString  
        })
        .done(function(data) {
            let response = document.getElementById('response');
            if (!data) {
                return;
            }

            if (langSwitch == "en") {
                response.classList.add('success_en');
                response.style.display = 'block';
                setTimeout(function(){
                    response.style.display = 'none';
                }, 3000);
            } else {
                response.classList.add('success_ru');
                response.style.display = 'block';
                setTimeout(function () { 
                    response.style.display = 'none';
                }, 3000);
            }
        })
        .fail(function() {      
            console.log( "Sending order failed. Something went wrong, try again !" );
        }); 

       return;
    };
    
    return {
        sendMessage: sendMessage,
        sendComment: sendComment
    }
})();

function sendMessage() {   
    formHandler.sendMessage();
}
function sendComment() {   
    formHandler.sendComment();
}