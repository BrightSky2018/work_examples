/*
* The code below is the front-end part of one of the projects. The code does 
* descriptive statistic analysis and provides graphical representation of data,
* as well as, dynamic numeric representation of data values min, max, sum etc, on
* zooming the graph. The code is not full, taken out of project context.
*/

var frontEndFunctions = (function () {
    window.scales = [];
    const minMaxSum = [];
    const ajaxResponse = [];
    const jobCategories = [];

    var displaySuccess = function (element, message) {
        let response = document.getElementById(element);

        response.innerHTML = (message);
        response.className += ('alert alert-success');
        response.style.display = 'block';

        setTimeout(function () {
            response.style.display = 'none';
        }, 3000);
    };

    var displayError = function (element, message) {
        let response = document.getElementById(element);

        response.innerHTML = (message);
        response.className += ('alert alert-danger');
        response.style.display = 'block';

        setTimeout(function () {
            response.style.display = 'none';
        }, 3000);
    };

    var eventOnPush = function (array, callback) {
        array.push = function (e) {
            Array.prototype.push.call(array, e);
            callback(array);
        };
    };

    var saveResults = function () {
        const url = '../analysis/save-results';
        ajaxRequest('POST', url, '');
    };

    var advancedSearch = function () {
        let button = document.getElementById('advanced-search');

        if (button.innerHTML === 'Open Advanced Search') {
            getJobGategory();
            document.getElementById('dates').style.display = 'block';
            button.innerHTML = 'Close Advanced Search';
        } else {
            cleanSkillStats("categories");
            document.getElementById('dates').style.display = 'none';
            button.innerHTML = 'Open Advanced Search';
        }
    };

    var ajaxRequest = function (method, url, formDataString) {    
        $.ajax({
            type: method,
            dataType: 'json',
            url: url,
            data: formDataString
        })
        .done(function (data) {
                   
            if (!data) {
                return;
            }      
           
            data.response ? ajaxResponse.push(data.response) : displaySuccess('response', data.message);                      
        })
        .fail(function () {
            displayError('response', 'Access denied you are not authorized !');
        });
    
        return;
    };

    var formatDate = function (date) {
        let newDate = new Date(date);
        let month = '' + (newDate.getMonth() + 1);
        let day = '' + newDate.getDate();
        let year = newDate.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    };

    var getGeneralCalculations = function (data) {
        let min = Math.min.apply(0, data.map(gd => { return gd.SkillCount; }));
        let max = Math.max.apply(0, data.map(gd => { return gd.SkillCount; }));
        let sum = data.reduce((acc, it) => { return acc + it.SkillCount; }, 0);
        let avg = Math.round(sum / data.length);
        let results = { min: min, max: max, avg: avg, sum: sum };

        return results;
    };

    var generateChartData = function (data) {
        let colors = [];
        let chartDataSets = {
            datasets: [

            ]
        };

        for (let i = 0; i < data.length; i++) {
            colors.push('#' + Math.floor(Math.random() * 16777215).toString(16));
        }

        for (let i = 0; i < data.length; i++) {
            let dataSet = {
                label: data[i][0].SkillName,
                backgroundColor: colors[i],
                borderColor: colors[i],
                hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                hoverBorderColor: 'rgba(200, 200, 200, 1)',
                data: data[i].map(gd => ({ x: formatDate(gd.CreatedAt), y: gd.SkillCount })),
                fill: false,
                showLine: true
            }

            let statData = getGeneralCalculations(data[i]);
            minMaxSum.push({
                skillName: data[i][0].SkillName,
                min: statData.min,
                max: statData.max,
                avg: statData.avg,
                sum: statData.sum
            });

            chartDataSets.datasets.push(dataSet);
        }

        return chartDataSets;
    };

    var displaySkillStats = function (data, minMaxSum) {
        var total = minMaxSum.reduce(function (acc, it) {
            return acc + it.sum;
        }, 0);

        for (let i = 0; i < data.length; i++) {
            document.querySelector('#skill-stats').insertAdjacentHTML(
                'beforeend',
                '<li class="d-sm-inline-block">' +
                '<p> Skill: ' + minMaxSum[i].skillName + '</p>' +
                '<p> Min: ' + minMaxSum[i].min + '</p>' +
                '<p> Max: ' + minMaxSum[i].max + '</p>' +
                '<p> Sum: ' + minMaxSum[i].sum + '</p>' +
                '<p> Avg: ' + minMaxSum[i].avg + '</p>' +
                '<p> Percentage: ' + Math.round(minMaxSum[i].sum / total * 100) + '%</p>' +
                '<p> Occurrence: ' + data[i].length + '</p>' +
                '</li>'
            );
        }
    };

    var cleanSkillStats = function (id) {
        let dynamicData = document.getElementById(id);

        while (dynamicData.firstChild) {
            dynamicData.removeChild(dynamicData.firstChild);
        }
    };

    var getJobGategory = function () {
        ajaxResponse.length = 0;
        jobCategories.length = 0;

        const url = '../analysis/get-categories';
        ajaxRequest('GET', url, '');

        eventOnPush(ajaxResponse, function (callbackArray) {            
            jobCategories.push(callbackArray[0]);
            console.log(callbackArray[0]);

            document.querySelector('#categories').insertAdjacentHTML(
                'beforeend',
                '<option value="">none</option>'
            );

            for (let i = 0; i < callbackArray[0].length; i++) {
                document.querySelector('#categories').insertAdjacentHTML(
                    'beforeend',
                    '<option value="' + callbackArray[0][i].name + '">' + callbackArray[0][i].name + '</option>'             
                );
            }
        });
    };

    var selectCategory = function () {
        let categoryName = document.getElementById('categories').value;
        let keyWords = document.getElementById('keywords');

        for (let i = 0; i < jobCategories[0].length; i++) {
            if (jobCategories[0][i].name == categoryName) {
                keyWords.value = jobCategories[0][i].skills.map(x => x.name).join(" ");
            } else {
                keyWords.value = '';
            }
        }    
    };

    var displayChart = function () {
        ajaxResponse.length = 0;

        const url = '../analysis/show-graph';
        ajaxRequest('GET', url, '');
       
        eventOnPush(ajaxResponse, function (callbackArray) {
            let parseGenericData = JSON.parse(callbackArray[0]);

            console.log(parseGenericData);
            let element = document.getElementById('chart-display');
            element.style.display = 'block';
     
            let chartData = generateChartData(parseGenericData);

            eventOnPush(scales, function (callbackArray) {
                minMaxSum.length = 0;

                if (typeof callbackArray[0] !== 'undefined') {
                   
                    if (typeof callbackArray[0].ticks !== 'undefined') {
                        var minDate = callbackArray[0].ticks[0];
                        var maxDate = callbackArray[0].ticks[callbackArray[0].ticks.length - 1];
                    }
                    
                    if (typeof minDate !== 'undefined') {
                        var filteredDates = [];
                        cleanSkillStats('skill-stats');
               
                        for (let i = 0; i < parseGenericData.length; i++) {
                             var betweenDates = parseGenericData[i].filter(el => {
                                 var realDate = el.CreatedAt;

                                 if (typeof el.SkillName !== 'undefined') {
                                     if (formatDate(realDate) >= formatDate(minDate) &&
                                         formatDate(realDate) <= formatDate(maxDate)) {

                                         return el;
                                     } 
                                  }                    
                             });
                             
                             filteredDates.push(betweenDates);
                             filteredDates = filteredDates.filter(e => e.length);                        
                        }
                       
                        for (let i = 0; i < filteredDates.length; i++) {
                            console.log(filteredDates[i]);
                            let statData = getGeneralCalculations(filteredDates[i]);

                            minMaxSum.push({
                                skillName: filteredDates[i][0].SkillName,
                                min: statData.min,
                                max: statData.max,
                                avg: statData.avg,
                                sum: statData.sum
                            });
                        }
                        
                        displaySkillStats(filteredDates, minMaxSum);
                    }        
                }
                            
                callbackArray.length = 0;            
            });

            displaySkillStats(parseGenericData, minMaxSum);

            let ctx = document.getElementById('skills-chart');

            let skillChart = new Chart(ctx, {
                type: 'line',
                data: chartData,
                options: {
                    title: {
                        display: true,
                        text: 'Skills occurance distrebution over the time',
                        fontSize: 31
                    },
                    scales: {
                        xAxes: [{
                            id: 'ScartCharxAxes',
                            type: 'time',
                            time: {
                                unit: 'day',
                                displayFormats: {
                                    day: 'MM/DD/YYYY'
                                },
                            }
                        }]
                    },
                    pan: {
                        enabled: true,
                        mode: 'x',
                    },
                    zoom: {
                        enabled: true,
                        mode: 'x',
                        speed: 0.1,
                    }
                }
            }); 

            skillChart.render(); 
        });
    };

    var closeChart = function () {
        let element = document.getElementById('chart-display');
        element.style.display = 'none';

        cleanSkillStats('skill-stats');
    };

    var closeEdit = function () {
        let element = document.getElementById('edit-filter');
        element.style.display = 'none';

        cleanSkillStats('edit-category');
    };

    var editCategory = function (element) {
        ajaxResponse.length = 0;

        const editId = element;
        const url = '../analysis/edit/' + editId;
        ajaxRequest('GET', url, '');

        eventOnPush(ajaxResponse, function (callbackArray) {
            jobCategories.push(callbackArray[0]);
            console.log(callbackArray[0][0].id);
        
            let element = document.getElementById('edit-filter');
            element.style.display = 'block';

            document.getElementById('edit-category-name').value = callbackArray[0][0].name;
            document.getElementById('category-id').value = callbackArray[0][0].id;

            for (let i = 0; i < callbackArray[0][0].skills.length; i++) {
                document.querySelector('#edit-category').insertAdjacentHTML(
                    'beforeend', '<li><input class="list-inline-item" type="text" name="edit-skill" value="' + callbackArray[0][0].skills[i].name + '"/>' +
                    '<button type="button" class="remove-skill" aria-label="Close" onclick="removeSkill(this)">' +
                    '<span aria-hidden="true">×</span></button ></li>'
                );
            }

            callbackArray[0].length = 0;         
        });
    };

    var removeSkill = function(input) {
        input.parentNode.remove()
    };

    var addSkill = function (element) {
        let name = '';
        element == '#category'  ? name = 'skill' : name = 'edit-skill';  

        document.querySelector(element).insertAdjacentHTML(
            'beforeend', '<li><input class="list-inline-item" type="text" name="' + name + '"/>' +
            '<button type="button" class="remove-skill" aria-label="Close" onclick="removeSkill(this)">' +
            '<span aria-hidden="true">×</span></button ></li>'
        );
    };

    return {
        saveResults: saveResults,
        editCategory: editCategory,
        displayChart: displayChart,
        selectCategory: selectCategory,
        getJobGategory: getJobGategory,
        closeChart: closeChart,
        closeEdit: closeEdit,
        advancedSearch: advancedSearch,
        removeSkill: removeSkill,
        addSkill: addSkill
    }
}) ();

function saveResults() {
    frontEndFunctions.saveResults();
}

function displayChart() {
    frontEndFunctions.displayChart();
}

function editCategory(element) {
    frontEndFunctions.editCategory(element);
}

function selectCategory() {
    frontEndFunctions.selectCategory();
}

function closeEdit() {
    frontEndFunctions.closeEdit();
}

function closeChart() {
    frontEndFunctions.closeChart();
}

function advancedSearch() {
    frontEndFunctions.advancedSearch();
}

function addSkill(element) {
    frontEndFunctions.addSkill(element);
}

function removeSkill(input) {
    frontEndFunctions.removeSkill(input);
}

window.onload = function () {
    document.getElementById("dates").style.display = 'none';
}