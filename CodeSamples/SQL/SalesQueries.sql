-- Q1

SELECT * 
FROM nr_sales s, nr_employees e, nr_products p, nr_categories ct, nr_customers cu, nr_regions r, nr_classes cl
WHERE
	s.employee_code = e.employee_code
	and s.customer_code = cu.customer_code
	and s.product_code = p.product_code
	and SUBSTR(p.product_code, 7, 1) = ct.category_code
	and cu.region = r.region
	and cu.cust_class = cl.cust_class;

-- or 

SELECT * 
FROM
nr_sales s
	JOIN nr_employees e   on s.employee_code = e.employee_code
	JOIN nr_customers cu  on s.customer_code = cu.customer_code
	JOIN nr_products p    on s.product_code = p.product_code
	JOIN nr_categories ct on SUBSTR(p.product_code, 7, 1) = ct.category_code
	JOIN nr_regions r     on cu.region = r.region
	JOIN nr_classes cl    on cu.cust_class = cl.cust_class;

-- Q2
	
SELECT
	SUBSTR(s.sales_code, 1, 8) "Sales Date"
	, SUM(s.qty * p.prod_price) "Revenue"
FROM
	nr_sales s
	JOIN nr_products p ON (s.product_code = p.product_code)
GROUP BY
	SUBSTR(s.sales_code, 1, 8)
ORDER BY
	SUBSTR(s.sales_code, 1, 8)
;

--Q3

SELECT
	TO_DATE(SUBSTR(s.sales_code, 1, 8), 'YYYYMMDD') "Sales Date"
	, TO_CHAR(SUM(s.qty * p.prod_price), '$999,999.00')	"Revenue"
FROM
	nr_sales s
	JOIN nr_products p ON (s.product_code = p.product_code)
GROUP BY
	SUBSTR(s.sales_code, 1, 8)
ORDER BY
	SUBSTR(s.sales_code, 1, 8)
;

--Q4

SELECT
	cu.region
	, SUM(s.qty * p.prod_price * ct.insurance)
FROM
	nr_sales s
	JOIN nr_customers cu ON (s.customer_code = cu.customer_code)
	JOIN nr_products p ON (s.product_code = p.product_code)
	JOIN nr_categories ct ON (SUBSTR(p.product_code, 7, 1) = ct.category_code)
GROUP BY
	region
;

--Q5

SELECT
	cat_description "Category",
	SUM(s.qty * p.prod_price) "Revenue"
FROM
	nr_sales s
	JOIN nr_products p ON (s.product_code = p.product_code)
	JOIN nr_categories ct ON (SUBSTR(p.product_code, 7, 1) =ct.category_code)
GROUP BY 
	cat_description
ORDER BY
	"Revenue" DESC
;

--Q6

SELECT
	cat_description "Category",
	SUM(s.qty * p.prod_price) "Revenue"
FROM
	nr_sales s
	JOIN nr_products p ON (s.product_code = p.product_code)
	JOIN nr_categories ct ON (SUBSTR(p.product_code, 7, 1) =ct.category_code)
GROUP BY 
	cat_description
ORDER BY
	DECODE (cat_description, 'Elegant', 1,
							 'Classic', 2,
							 'Medium', 3,
							 4)
;

--Q7

CREATE OR REPLACE VIEW nr_sales_v1 AS 
SELECT
	cu.region
	, SUM(s.qty * p.prod_price) revenue
FROM
	nr_sales s
	JOIN nr_customers cu ON (s.customer_code = cu.customer_code)
	JOIN nr_products p ON (s.product_code = p.product_code)
GROUP BY
	cu.region
ORDER BY
	SUM(s.qty * p.prod_price) DESC
;	

--Q7

SELECT
	emp_name
	, SUM(s.qty * p.prod_price * r.commission)
FROM
	nr_sales s
	JOIN nr_employees e ON (s.employee_code = e.employee_code)
	JOIN nr_products p ON (s.product_code = p.product_code)
	JOIN nr_customers cu ON (s.customer_code = cu.customer_code)
	JOIN nr_regions r ON (cu.region = r.region)
WHERE
	cu.region = (SELECT region FROM nr_sales_v1 WHERE ROWNUM = 1)
GROUP BY 
	emp_name
ORDER BY
	emp_name
;

--Q8

SELECT
	emp_name
	, cust_class
	, SUM(s.qty * p.prod_price * r.commission) Commission
FROM
	nr_sales s
	JOIN nr_employees e ON (s.employee_code = e.employee_code)
	JOIN nr_products p ON (s.product_code = p.product_code)
	JOIN nr_customers cu ON (s.customer_code = cu.customer_code)
	JOIN nr_regions r ON (cu.region = r.region)
WHERE
	cu.region = (SELECT region FROM nr_sales_v1 WHERE ROWNUM = 1)
GROUP BY 
	emp_name, cust_class
ORDER BY
	emp_name, cust_class
;

-- Q9

SELECT
	emp_name
	, SUM(DECODE(cust_class, 'A', s.qty * p.prod_price * r.commission, 0)) "Class A"
	, SUM(DECODE(cust_class, 'B', s.qty * p.prod_price * r.commission, 0)) "Class B"
	, SUM(DECODE(cust_class, 'C', s.qty * p.prod_price * r.commission, 0)) "Class C"
	, SUM(DECODE(cust_class, 'D', s.qty * p.prod_price * r.commission, 0)) "Class D"
FROM
	nr_sales s
	JOIN nr_employees e ON (s.employee_code = e.employee_code)
	JOIN nr_products p ON (s.product_code = p.product_code)
	JOIN nr_customers cu ON (s.customer_code = cu.customer_code)
	JOIN nr_regions r ON (cu.region = r.region)
WHERE
	cu.region = (SELECT region FROM nr_sales_v1 WHERE ROWNUM = 1)
GROUP BY 
	emp_name
ORDER BY
	emp_name
;
