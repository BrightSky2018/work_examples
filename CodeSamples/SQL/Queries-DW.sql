INSERT INTO dim_employee 
(
    employee_id, 
    full_name, 
    gender, 
    contract, 
    address
)
SELECT 
    human_resource_department.employees.employee_id,
    CONCAT(first_name, ' ', last_name) as full_name,
    SUBSTR(human_resource_department.employees.gender ,1, 1),
    human_resource_department.employees.contract_type,
    human_resource_department.employees.address
FROM 
    human_resource_department.employees 
WHERE
    human_resource_department.employees.employee_id NOT IN (
        SELECT 
            dim_employee.employee_id
        FROM
            dim_employee
    )
;

INSERT INTO dim_project 
(
    project_id, 
    scope, 
    level, 
    trade
) 
SELECT 
    construction_department.projects.project_id,
    construction_department.projects.scope,
    construction_department.projects.level,
    construction_department.projects.trade
FROM 
    construction_department.projects 
WHERE
    construction_department.projects.project_id NOT IN (
        SELECT 
            dim_project.project_id
        FROM
            dim_project
    )
;

INSERT INTO dim_boq 
(
    boq_id, 
    boq_category, 
    boq_name
)
SELECT 
    construction_department.boq.boq_id,
    construction_department.boq.boq_category,
    construction_department.boq.boq_name
FROM 
    construction_department.boq
WHERE
    construction_department.boq.boq_id NOT IN (
        SELECT 
            dim_boq.boq_id 
        FROM
            dim_boq
    )
;

INSERT INTO dim_construction 
(
    construction_id, 
    construction_type, 
    construction_family, 
    construction_part
) 
SELECT 
    construction_department.construction.construction_id,
    construction_department.construction.construction_type,
    construction_department.construction.construction_family,
    construction_department.construction.construction_part
FROM 
    construction_department.construction 
WHERE 
    construction_department.construction.construction_id NOT IN (
        SELECT 
            dim_construction.construction_id
        FROM
            dim_construction
    )
;

INSERT INTO dim_supplier 
(
    supplier_id, 
    supplier_name, 
    contract
)
SELECT 
    accounting_department.suppliers.supplier_id,
    accounting_department.suppliers.company_name,
    accounting_department.suppliers.contract
FROM 
    accounting_department.suppliers
WHERE
    accounting_department.suppliers.supplier_id NOT IN (
        SELECT 
            dim_supplier.supplier_id 
        FROM
            dim_supplier
    )
;
