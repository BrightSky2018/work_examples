<?php

namespace FileHandler;

interface FileInterface
{
    public function create();
    public function read();
    public function write($data);
    public function delete();
    public function clean();
}