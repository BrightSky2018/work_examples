<?php 

//including namespaces
include ('FileHandlerException.php');
include ('FileInterface.php');
include ('AbstractFile.php');
include ('TextFile.php');
include ('JsonFile.php');
include ('CsvFile.php');

use FileHandler\Exceptions\FileHandlerException;
use FileHandler\TextFile;
use FileHandler\JsonFile;
use FileHandler\CsvFile;

try {
   $file = new CsvFile('test.csv', 'a');

   $file->create();
   $file->write(['test 1', 'Test 3']);
   $file->write(['test 2', 'Test 4']);
   var_dump($file->read());
   $file->clean();
   $file->delete();
} catch (FileHandlerException $e) {
    echo $e->getMessage();

    $file->write($e->getMessage());
    $file->write($e->getTraceAsString());
}

try {
    $file = new TextFile('test.txt', 'w+');

    $file->create();
    $file->write('Test 123');
    $file->write('Test 345');
    print_r($file->read());
    $file->clean();
    $file->delete();
} catch (FileHandlerException $e) {
    echo $e->getMessage();

    $file->write($e->getMessage());
    $file->write($e->getTraceAsString());
}

try {
    $jsonFile = new JsonFile('test.json', 'w', JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

    $test = [
        'first_name' => 'Test', 
        'last_name'=>'Test',
        'birth_date'=> '0.0.0',
        'phone_number' => '00000'
    ];
    
    $jsonFile->create();
    $jsonData = $jsonFile->read();
    
    if (!empty($jsonData)) {
        array_push($jsonData, $test);
        $jsonFile->write($jsonData);
    } else {
        $jsonFile->write([$test]);
    }
    
    var_dump($jsonFile->read()); 
    $jsonFile->clean();
    $jsonFile->delete();
} catch (FileHandlerException $e) {
    echo $e->getMessage();
}
