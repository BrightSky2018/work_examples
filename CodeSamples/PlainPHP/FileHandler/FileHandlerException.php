<?php

namespace FileHandler\Exceptions;

use Exception;

final class FileHandlerException extends Exception
{
}