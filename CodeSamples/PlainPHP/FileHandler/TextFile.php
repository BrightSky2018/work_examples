<?php declare(strict_types=1);

namespace FileHandler;

use FileHandler\Exceptions\FileHandlerException;

class TextFile extends AbstractFile
{
    public function __construct(string $file, string $mode)
    {
        parent::__construct($file, $mode);
    }
 
    /**
     * Checks if the file exist and reads it content.
     * 
     * @return array|bool
     * @throws FileHandlerException
     */
    public function read()
    {
        if (!is_readable($this->file)) {
            throw new FileHandlerException(sprintf('File "%s" is either not readable or does not exist !', $this->file));
        }       

        $file = fopen($this->file, 'r');
        $content = fread($file, filesize($this->file));
        fclose($file);
        
        if (!$content) {
            throw new FileHandlerException(sprintf('Could not read from file "%s" - it is empty !', $this->file));
        }

        return explode('\n\n', $content);
    }

    /**
     * Checks if the file exist and writes content in to it.
     * 
     * @param string $content Information to be written to the file
     * @param bool $lock Flag, weather to lock file or not during the write operaton
     * @return bool
     * @throws FileHandlerException
     */
    public function write($content, $lock = false): bool
    {
        if (!is_writable($this->file)) {
            throw new FileHandlerException(sprintf('File "%s" is either not writable or does not exist !', $this->file));
        } 
             
        $file = fopen($this->file, $this->mode);
        
        if (!$lock) {
            $write = fwrite($file, $content);
        } else {
            if (flock($file, LOCK_EX)) {
                $write = fwrite($file, $content);
                flock($file, LOCK_UN);
            } else {
                throw new FileHandlerException(sprintf('Could not lock file "%s", try to write again !', $this->file));
            }
        }

        fclose($file);

        if (!$write) {
            throw new FileHandlerException(sprintf('Could not write into file "%s" !', $this->file));
        }
        
        return true;
    }
}
