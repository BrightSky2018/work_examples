<?php

declare(strict_types=1);

namespace FileHandler;

use FileHandler\Exceptions\FileHandlerException;

abstract class AbstractFile implements FileInterface
{
    /**
     * @var string
     */
    protected $file;

    /**
     * @var string
     */
    protected $mode;

    abstract public function read();
    abstract public function write($data): bool;

    public function __construct(string $file, string $mode)
    {
        $this->file = $file;
        $this->mode = $mode;
    }

    /**
     * Checks file and creates if it does not exist.
     * 
     * @return bool
     * @throws FileHandlerException
     */
    public function create(): bool
    {
        if (file_exists($this->file)) {
            throw new FileHandlerException(sprintf('Could not create file "%s", it already exists !', $this->file));
        }

        $file = fopen($this->file, $this->mode);

        if (!$file) {
            throw new FileHandlerException(sprintf('Could not create file "%s" !', $this->file));
        }

        fclose($file);

        return true;
    }

    /**
     * Checks file and deletes if it exists.
     * 
     * @return bool
     * @throws FileHandlerException
     */
    public function delete(): bool
    {
        if (!file_exists($this->file)) {
            throw new FileHandlerException(sprintf('File "%s" does not exist !', $this->file));
        }

        if (!unlink($this->file)) {
            throw new FileHandlerException(sprintf('Could not delete file "%s" !', $this->file));
        }

        return true;
    }

    /**
     * Checks file and truncates if it exists.
     * 
     * @return bool
     * @throws FileHandlerException
     */
    public function clean(): bool
    {
        if (!file_exists($this->file)) {
            throw new FileHandlerException(sprintf('File "%s" does not exist !', $this->file));
        }

        $file = fopen($this->file, $this->mode);
        $truncate = ftruncate($file, 0);
        fclose($file);

        if (!$truncate) {
            throw new FileHandlerException(sprintf('Failed to truncate file "%s" !', $this->file));
        }

        return true;
    }
}
