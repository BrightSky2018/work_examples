<?php declare(strict_types=1);

namespace FileHandler;

use FileHandler\Exceptions\FileHandlerException;

class JsonFile extends AbstractFile
{
    /**
     * @var int
     */
    private $flags;

    public function __construct(string $file, string $mode, int $flags = 0)
    {
        parent::__construct($file, $mode);
        $this->flags = $flags;
    }

    /**
     * Checks if the file exist and reads it content.
     * 
     * @return mixed
     * @throws FileHandlerException
     */
    public function read()
    {
        if (!is_readable($this->file)) {
            throw new FileHandlerException(sprintf('File "%s" is either not readable or does not exist !', $this->file));
        }  

        $content = file_get_contents($this->file);

        return json_decode($content, true);
    }

    /**
     * Checks if the file exist and writes content in to it.
     * 
     * @param array $content Information to be written to the file
     * @param bool $lock Flag, weather to lock file or not during the write operaton
     * @return bool
     * @throws FileHandlerException
     */
    public function write($content, $lock = false): bool
    {
        if (!is_writable($this->file)) {
            throw new FileHandlerException(sprintf('File "%s" is either not writable or does not exist !', $this->file));
        }         

        $encode = json_encode($content, $this->flags);
        $file = fopen($this->file, $this->mode);

        if (!$lock) {
            $write = fwrite($file, $encode);
        } else {
            if (flock($file, LOCK_EX)) {
                $write = fwrite($file, $encode);
                flock($file, LOCK_UN);
            } else {
                throw new FileHandlerException(sprintf('Could not lock file "%s", try to write again !', $this->file));
            }
        }

        fclose($file);

        if (!$write) {
            throw new FileHandlerException(sprintf('Could not write into file "%s" !', $this->file));
        }

        return true;
    }
}
