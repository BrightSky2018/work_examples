<?php declare(strict_types=1);

namespace FileHandler;

use FileHandler\Exceptions\FileHandlerException;

class CsvFile extends AbstractFile
{
    /**
     * @var string
     */
    private $delimiter;

    public function __construct(string $file, string $mode, string $delimiter = ',', array $options = [])
    {
        parent::__construct($file, $mode);
        $this->delimiter = $delimiter;
        $this->options = $options;
    }

    /**
     * Checks if the file exist and reads it content.
     * 
     * @return array
     * @throws FileHandlerException
     */
    public function read(): array
    {
        if (!is_readable($this->file)) {
            throw new FileHandlerException(sprintf('File "%s" is either not readable or does not exist !', $this->file));
        }

        $content = [];
        $file = fopen($this->file, 'r');

        while ($data = fgetcsv($file, 1000, ',')) {
            for ($i = 0; $i < count($data); $i++) {
                array_push($content, $data[$i]);
            }
        }

        fclose($file);

        if (!$content) {
            throw new FileHandlerException(sprintf('Could not read from file "%s" - it is empty !', $this->file));
        }

        return $content;
    }

    /**
     * Checks if the file exist and writes content in to it.
     * 
     * @param array $content Information to be written to the file
     * @param bool $lock Flag, weather to lock file or not during the write operaton
     * @return bool
     * @throws FileHandlerException
     */
    public function write($content, $lock = false): bool
    {
        if (!is_writable($this->file)) {
            throw new FileHandlerException(sprintf('File "%s" either not writable or does not exist !', $this->file));
        }

        $file = fopen($this->file, $this->mode);

        if (!$lock) {
            $write = fputcsv($file, $content, $this->delimiter);
        } else {
            if (flock($file, LOCK_EX)) {
                $write = fputcsv($file, $content, $this->delimiter);
                flock($file, LOCK_UN);
            } else {
                throw new FileHandlerException(sprintf('Could not lock file "%s", try to write again !', $this->file));
            }
        }

        fclose($file);

        if (!$write) {
            throw new FileHandlerException(sprintf('Could not write into file "%s" !', $this->file));
        }

        return true;
    }
}
