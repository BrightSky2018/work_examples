<?php

declare(strict_types=1);

namespace WeatherApp\Library;

/**
 * Base/parent template class for all possible variations of the classes
 * which use HTTP request protocol. Provides the initial/base request
 * 
 * NOTE: In case of need the class and the interface it extends, can 
 * be enlarged with an additional set of functionality
 */

use WeatherApp\Exceptions\HttpRequestException;
use WeatherApp\Library\Interfaces\RequestInterface;

abstract class HttpRequest implements RequestInterface
{
    protected const METHOD_GET = 'GET';
    protected const METHOD_POST = 'POST';
    protected const METHOD_PUT = 'PUT';
    protected const METHOD_DELETE = 'DELETE';

    public function __construct()
    {
    }

    /**
     * Initializes and executes curl request, based on the passed settings parameters.
     * 
     * @param array $settings an assoc. array <key, val> of settings to be passed to the curl_setopt_array()
     * @return string|bool
     * @throws HttpRequestException
     */
    public function baseRequest(array $settings)
    {
        if (!$settings) {
            throw new HttpRequestException('Request settings can not be compleatly empty, at least CURLOPT_URL has to be passed.');
        }

        if (!isset($settings[CURLOPT_RETURNTRANSFER])) {
            $settings[CURLOPT_RETURNTRANSFER] = true;
        }

        $curl = curl_init();
        curl_setopt_array($curl, $settings);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            throw new HttpRequestException('CURL Error #:' . $err);
        } else {
            return $response;
        }
    }
}
