<?php

namespace WeatherApp\Library\Interfaces;

/**
 * The interface to outline base rules for all types of requests one
 * would wish to use in the project i.e http or tcp.
 * 
 * NOTE: In fact if a project does not grow much, an interface is overkill. 
 * If one wants to create a template class and to enforce some common
 * functionality then the abstract Class is more than enough
 */

interface RequestInterface
{
    public function baseRequest(array $settings);
    public function getRequest(string $url, array $data = []): ?array;
}