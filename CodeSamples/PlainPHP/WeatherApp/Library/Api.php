<?php

declare(strict_types=1);

namespace WeatherApp\Library;

/**
 * The weather app api wrap up class. Designed to provide all the
 * necessary functionality on the upper code abstraction level
 * for reqeust/response exchange with - https://api.openweathermap.org/data/2.5/weather
 * 
 * NOTE: Of course for the current project no need to implement the whole set of request methods
 * i.e post, put, delete.
 */

final class Api extends HttpRequest
{
    public function __construct()
    {
    }

    /**
     * Executes get request to a specified url and returns either results or null
     * 
     * @param string $url a url to make request to
     * @param array $data data in fromat of an assoc. array <key, val>, which is used as in query parameters
     * @return array|null
     */
    public function getRequest(string $url, array $data = []): ?array
    {
        if ($data) {
            $url = $url . '?' . http_build_query($data);
        }

        $options = [
            CURLOPT_URL => $url,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_CUSTOMREQUEST => self::METHOD_GET
        ];

        $reqeust = parent::baseRequest($options);

        return $reqeust ? json_decode($reqeust, true) : null;
    }

    /*
    public function postRequest(string $url, array $data = []): array
    {
    }

    public function putRequest(string $url, array $data = []): array
    {
    }

    public function deleteRequest(string $url, array $data = []): array
    {
    }
    */
}
