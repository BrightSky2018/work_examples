<?php

declare(strict_types=1);

namespace WeatherApp\Library;

/**
 * A helper class, which loads all the nescessary files to a directory,
 * where the Autoloader class is included and called from.
 * 
 * NOTE: Ideally in a larger size projects, better to use spl_autoload_register()
 */

final class Autoloader
{

    /**
     * Loads all the files from the specified directories
     * 
     * @return void
     */
    public static function load(array $directories): void
    {
        $dir =  realpath(__DIR__ . '/..') . '/';

        foreach ($directories as $directory) {
            if (is_readable($dir . $directory . '.php')) {
                require_once($dir . $directory . '.php');
            }
        }
    }
}
