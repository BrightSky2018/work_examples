<?php

declare(strict_types=1);

namespace WeatherApp\Library;

/**
 * A small and handy helper class to parse .env files, so that content
 * becomes accessable as environment variables.
 * 
 * NOTE: Quite usefull class, with straight forward and well known across the
 * web implementation. In a larger project could be a part of the file handler
 * classes and extend one of the base/parent template classes.
 */

use WeatherApp\Exceptions\ConfigException;

final class Config
{

    /**
     * Parses .env files and assignes its content to the corresponding $_ENV and $_SERVER variables
     * which have global scope.
     * 
     * @param string $path path to the .env file to parse
     * @return void
     */
    public static function load(string $path): void
    {
        if (!is_readable($path)) {
            throw new ConfigException($path . ' file is not readable');
        }

        $lines = file($path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        foreach ($lines as $line) {
            if (strpos(trim($line), '#') === 0) {
                continue;
            }

            list($name, $value) = explode('=', $line, 2);
            $name = trim($name);
            $value = trim($value);

            if (!array_key_exists($name, $_SERVER) && !array_key_exists($name, $_ENV)) {
                putenv(sprintf('%s=%s', $name, $value));
                $_ENV[$name] = $value;
                $_SERVER[$name] = $value;
            }
        }
    }
}
