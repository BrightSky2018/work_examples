## Description
The project is developed using vanilla PHP. It was decided purposely to avoid using
any composer imported packages. If to import right packages the project can easily become
almost indistinguishable from the one implemented with help of a framewrok.

NOTE: Since the task description is vague (lacks specifics and details) and can be interpreted in various ways,
therefore I implemented the way I saw fit.

## How to use ?
 - start server `php -S localhost:8084 index.php`
 - run from the browser `http://localhost:8084/index.php?city=Rome`
 - run from CLI `php weather Rome`
 - run from CLI `php Tests/Tests.php`

## How to use with docker ?
 - build image `docker build --tag=<image name> .`
 - run docekr image `docker run -p 8084:80 <image name>`
 - run from the browser `http://localhost:8084/index.php?city=Rome`
 - connect to the container `sudo docker exec -it <container name> bash`
 - run from CLI `php weather Rome`
 - run from CLI `php Tests/Tests.php`