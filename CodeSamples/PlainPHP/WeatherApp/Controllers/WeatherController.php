<?php

declare(strict_types=1);

namespace WeatherApp\Controllers;

/**
 * The weather api controller class. Since the application follows
 * MVC arhitectural design pattern then the implementation of such class,
 * or any other with similar functionality is a must.
 * 
 * NOTE: Ideally a controller class acts as a mediator between all code levels of an application. 
 * Ususally, a controller class invokes models, library classes and helpers
 * to perform curtain functionality and returns aggregated/finalized results.
 */

use WeatherApp\Objects\Weather;
use WeatherApp\Exceptions\WeatherControllerException;
use WeatherApp\Library\Interfaces\RequestInterface;

final class WeatherController
{
    /**
     * @var RequestInterface
     */
    private $client;

    public function __construct(RequestInterface $client)
    {
        $this->client = $client;
    }

    /**
     * Accepts a city name as a parameter and return weather information.
     * NOTE: Views are not necessary in the current project, thus raw string is returned.
     * 
     * @param string $city
     * @return string|null
     */
    public function getWeather(string $city): ?string
    {
        $result = null;

        try {
            $data = $this->client->getRequest(getenv('API_URL'), ['q' => $city, 'appid' => getenv('API_KEY')]);

            // if there is response data and its code is 200 prepare
            // weather forecast, otherwise stringify the error
            if ($data && $data['cod'] == 200) {
                $weatherObj = new Weather($data);
                $result = $weatherObj->getDescription() . ', ' . $weatherObj->getTemperature() . ' degrees celcius';
            } else {
                $result = json_encode($data);
            }
        } catch (\Throwable $e) {
            // Can either re-throw another exception as the return result to a client
            // or log. However, in the current case it is just returned, for sake of example.
            // throw new WeatherControllerException('Some message);
            $result = $e->getMessage();
        }

        return $result;
    }
}
