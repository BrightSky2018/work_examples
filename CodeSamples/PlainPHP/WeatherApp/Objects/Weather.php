<?php

declare(strict_types=1);

namespace WeatherApp\Objects;

/**
 * A generic wrap up class/object around json array/object response from the
 * weather api, in order easier to store and manipulate response
 * inormation. If the project was large enought then there most likely would 
 * be some parent model to extend, while the current project does not need that.
 * 
 * NOTE: Ideally all data which circulates inside an app should be represented
 * as an object.
 */

final class Weather
{
    /**
     * constant required for conversion 
     * info - https://en.wikipedia.org/wiki/Kelvin 
     */
    private const KELVIN_CONSTANT = 273.15;

    public function __construct(array $data = [])
    {
        if ($data) {
            $this->set($data);
        }
    }

    /**
     * Creates class local properties and set their values from json array/object
     * 
     * @param array $data a json array/object
     * @return void
     */
    public function set(array $data): void
    {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $sub = new Weather;
                $sub->set($value);
                $value = $sub;
            }
            $this->{$key} = $value;
        }
    }

    /**
     * Returns weather condition description. If there are multiple
     * descriptions, then returns them comma separated
     * 
     * @return string
     */
    public function getDescription(): string
    {
        $description = '';

        foreach ($this->weather as $key => $value) {
            if ($description) {
                $description .= ', ';
            }

            $description .= $value->description;
        }

        return $description;
    }

    /**
     * Converts temperature from Kelvin to Celcius
     * 
     * @return int|float
     */
    public function getTemperature()
    {
        return $this->main->temp - self::KELVIN_CONSTANT;
    }
}
