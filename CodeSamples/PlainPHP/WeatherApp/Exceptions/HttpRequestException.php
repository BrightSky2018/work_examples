<?php

namespace WeatherApp\Exceptions;

use Exception;

final class HttpRequestException extends Exception
{
}
