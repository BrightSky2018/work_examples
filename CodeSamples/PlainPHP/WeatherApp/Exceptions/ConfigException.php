<?php

namespace WeatherApp\Exceptions;

use Exception;

final class ConfigException extends Exception
{
}
