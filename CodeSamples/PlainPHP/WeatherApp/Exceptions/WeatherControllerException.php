<?php

namespace WeatherApp\Exceptions;

use Exception;

final class WeatherControllerException extends Exception
{
}
