<?php

declare(strict_types=1);

include('Library/Autoloader.php');

use WeatherApp\Library\Autoloader;
use WeatherApp\Library\Config;
use WeatherApp\Library\Api;
use WeatherApp\Controllers\WeatherController;

$directories = [
    'Exceptions/WeatherControllerException',
    'Exceptions/ConfigException',
    'Library/Interfaces/RequestInterface',
    'Library/Config',
    'Exceptions/HttpRequestException',
    'Library/HttpRequest',
    'Library/Api',
    'Objects/Weather',
    'Controllers/WeatherController'
];

Autoloader::load($directories);
Config::load(__DIR__ . '/.env');

$app = new WeatherController(new Api);

/**
 * Ideally if that was an online available, public project
 * the input had to be sanitized and checked. For the current
 * project no need to use any of that.
 */
if (isset($argv[1])) {
    echo $app->getWeather($argv[1]);
}

if (isset($_GET['city'])) {
    echo $app->getWeather($_GET['city']);
}
