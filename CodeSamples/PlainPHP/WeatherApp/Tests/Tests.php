<?php

declare(strict_types=1);

namespace WeatherApp\Tests;

/**
 * The class is designed to test weather app functionality.
 * 
 * NOTE: Do not try to find much sense in all of the test cases,
 * since most are done for sake of showing the ability to test.
 * 
 * In fact in the MVC projects with functionality predominantly based on API calls,
 * it is better to test a project using integration testing. Might look far fetched,
 * however testSequentialCalls() is an example of such test on a very small scale.
 * It hits all the fucntionality of the weather app and if all works fine it returns
 * results, otherwise it crushes with erros.
 * 
 * Isolated, unit testing is another type of testing and fits for an arithmetic functionality,
 * helpers classes/functions where a tester can actually change input and get different
 * output for compareson. testKelvinTOCelcius() is a good example of such test,
 * though the parameter is a property of the class and not a direct input as an argument.
 * 
 * In a larger size project, most likely the test class had to extend some base/parent template
 * class to inherit db conenction or any other used functionality for testing.
 */

include('Library/Autoloader.php');

use WeatherApp\Library\Autoloader;
use WeatherApp\Library\Config;
use WeatherApp\Objects\Weather;
use WeatherApp\Library\Api;
use WeatherApp\Library\Interfaces\RequestInterface;
use WeatherApp\Controllers\WeatherController;

$directories = [
    'Exceptions/WeatherControllerException',
    'Exceptions/ConfigException',
    'Library/Interfaces/RequestInterface',
    'Library/Config',
    'Exceptions/HttpRequestException',
    'Library/HttpRequest',
    'Library/Api',
    'Objects/Weather',
    'Controllers/WeatherController'
];

Autoloader::load($directories);
Config::load(realpath(__DIR__ . '/..') . '/.env');

final class Tests
{
    /**
     * @var RequestInterface
     */
    private $client;

    public function __construct(RequestInterface $client)
    {
        $this->client = $client;
    }

    /**
     * Test if config imported successfully
     * 
     * @return bool
     */
    public function testConfigSuccess(): bool
    {
        try {
            Config::load(__DIR__ . '/../.env');
        } catch (\Throwable $e) {
            return false;
        }

        return true;
    }

    /**
     * Test if config import fails on invalid filename
     * 
     * @return bool
     */
    public function testConfigFail(): bool
    {
        try {
            Config::load(__DIR__ . '/.envs');
        } catch (\Throwable $e) {
            if ($e instanceof \Exception) {
                return true;
            }
        }

        return false;
    }

    /**
     * Test if the weather api call is successful, by cross checking
     * returned data with the one was sent.
     * 
     * @return bool
     */
    public function testApiCallSuccess(): bool
    {
        try {
            $data = $this->client->getRequest(getenv('API_URL'), ['q' => 'London', 'appid' => getenv('API_KEY')]);
            $weatherObj = new Weather($data);

            if ($weatherObj->name !== 'London' && !is_numeric($weatherObj->main->temp)) {
                return false;
            }
        } catch (\Throwable $e) {
            return false;
        }

        return true;
    }

    /**
     * Test Kelvin to Celsius conversion
     * 
     * @return bool
     */
    public function testKelvinToCelcius(): bool
    {
        try {
            $data = $this->client->getRequest(getenv('API_URL'), ['q' => 'London', 'appid' => getenv('API_KEY')]);
            $weatherObj = new Weather($data);
            $weatherObj->main->temp = 273.15;

            if ($weatherObj->getTemperature() != 0) {
                return false;
            }
        } catch (\Throwable $e) {
            return false;
        }

        return true;
    }

    /**
     * Test to check if the exception on empty settings is thrown
     * 
     * @return bool
     */
    public function testCrushOnEmptySettings(): bool
    {
        try {
            $this->client->baseRequest([]);
        } catch (\Throwable $e) {
            if (strpos($e->getMessage(), 'CURLOPT_URL has to be passed') !== false) {
                return true;
            }
        }

        return false;
    }

    /**
     * Test to check if curl crushes if api call was with invalid settings
     * 
     * @return bool
     */
    public function testCrushOnInvalidSettings(): bool
    {
        try {
            $options = [
                CURLOPT_URL => 'httpssss://test.com'
            ];

            $this->client->baseRequest($options);
        } catch (\Throwable $e) {
            if (strpos($e->getMessage(), 'CURL') !== false) {
                return true;
            }
        }

        return false;
    }

    /**
     * Tests that the whole app works, by returning results instead of null
     * 
     * @return bool
     */
    public function testSequentialCalls(): bool
    {
        $app = new WeatherController($this->client);

        $cities = ['Moscow', 'Berlin', 'London', 'New York'];

        foreach ($cities as $city) {
            if (!$app->getWeather($city)) {
                return false;
            }
            echo $app->getWeather($city) . "\n";
        }

        return true;
    }
}

$class = new Tests(new Api);
$methods = get_class_methods($class);

foreach ($methods as $method) {
    if ($method !== '__construct') {
        if ($class->{$method}()) {
            echo $method . " passed\n";
        } else {
            echo $method . " failed\n";
        }
    }
}
