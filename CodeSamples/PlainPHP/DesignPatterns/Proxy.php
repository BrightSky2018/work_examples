<?php declare(strict_types=1);

/*
* Example of one of the structural patterns
*/
class MoviesContainer 
{
    private $movies = [];

    public function __construct() 
    {
    }

    public function addMovie(Movie $movie): string
    {
        $this->movies[] = $movie;

        return 'Movie has been added !';
    }

    public function deleteMovie(Movie $movie): string
    {
        for ($i = 0; $i <= $this->getCount(); $i++) {
            if ($movie->getTitle() == $this->movies[$i]->getTitle()) {
                unset($this->movies[$i]);

                return 'The movies has been deleted !';
            }    
        }
    }

    public function findMovie(Movie $movie): Movie
    {
        for ($i = 0; $i <= $this->getCount(); $i++) {
            if ($movie->getTitle() == $this->movies[$i]->getTitle()) {
                return $this->movies[$i];
            }    
        }
    }

    public function listMovies(): array
    {
        return $this->movies;
    }

    public function getCount(): int 
    {
        return count($this->movies);
    }
}

class Movie 
{
    private $title;
    private $producer;

    public function __construct($title, $producer)
    {
        $this->title  = $title;
        $this->producer = $producer;  
    }

    public function getProducer(): string
    {
        return $this->producer;
    }

    public function getTitle(): string 
    {
        return $this->title;
    }
}

class ProxyMoviesContainer 
{
    private $moviesContainer; 

    public function __construct(MoviesContainer $moviesContainer) 
    {
        $this->moviesContainer = $moviesContainer;
    }

    public function addMovie(Movie $movie): string
    {
        return $this->moviesContainer->addMovie($movie);
    }  

    public function findMovie($movie): Movie
    {
        return $this->moviesContainer->findMovie($movie);
    }

    public function deleteMovie($movie): string
    {
        return $this->moviesContainer->deleteMovie($movie);
    }

    public function listMovies(): array 
    {
        return $this->moviesContainer->listMovies();
    }

    public function getCount(): int 
    {
        return $this->moviesContainer->getCount();
    }
}

$proxy = new ProxyMoviesContainer(new MoviesContainer());
$movie0 = new Movie('Guardians of The Galaxy','Kevin Feige');
$movie1 = new Movie('Avenges End Game','Russo Brothers');

echo $proxy->addMovie($movie0) . '<br>';
echo $proxy->addMovie($movie1) . '<br>';

echo $proxy->getCount() . '<br>';
echo $proxy->deleteMovie($movie1) . '<br>';

$findMovie = $proxy->findMovie($movie0);
echo $findMovie->getTitle() . '<br>';

echo '<pre>';
var_dump($proxy->listMovies());
echo '</pre>';