<?php declare(strict_types=1);

/*
* Example of one of the creational patterns
*/
interface PlaneFactoryInterface 
{
    public static function factoryCreate($plane);
}

abstract class AbstractPlane
{
    protected $type;
 
    public function getType(): string
    {
        return $this->type;
    }
}
 
class Tu160 extends AbstractPlane 
{
    public function __construct()
    {
        $this->type = 'Stratigic bomber';
    }
}
 
class Su27 extends AbstractPlane
{
    public function __construct()
    {
        $this->type = 'Fighter Jet';
    }
}
 
class PlaneFactory implements PlaneFactoryInterface 
{
    public static function factoryCreate($plane): object 
    {
        switch ($plane) {
            case 'Tu160':
                if (class_exists($plane)) {
                    $obj = new Tu160();
                }              
                break;
            case 'Su27':
                if (class_exists($plane)) {
                    $obj = new Su27();
                }              
                break;
            default:
                throw new Exception('Factory could not create plane type ' . $plane . ' does not exist !');
        }
        
        return $obj;
    }
}
 
try {
    $plane1 = PlaneFactory::factoryCreate('Tu160');
    echo $plane1->getType() . '<br>';
 
    $plane2 = PlaneFactory::factoryCreate('Su27');
    echo $plane2->getType() . '<br>'; 

    $plane3 = PlaneFactory::factoryCreate('Su22s');
    echo $plane3->getType() . '<br>'; 
} catch (Exception $e) {
    echo $e->getMessage();
}
