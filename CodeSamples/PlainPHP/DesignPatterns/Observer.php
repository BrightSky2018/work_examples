<?php declare(strict_types=1);

/*
* Example of one of the behavioral patterns
*/
abstract class Observer
{
    public function update($subject): void
    {
        if (method_exists($this, $subject->getState())) {
            call_user_func_array([$this, $subject->getState()], [$subject]);
        } 
    }
}

abstract class Subject
{
    protected $observers;
    protected $state;
    protected $content;

    public function __construct() 
    {
        $this->observers = [];
        $this->state = null;
    }

    public function attach(Observer $observer): void 
    {
        if (!array_search($observer, $this->observers)) {
            $this->observers[] = $observer;
        }
    }

    public function detach(Observer $observer): void
    {
        if (!empty($this->observers)) {
            for ($i = 0; $i < count($this->observers); $i++) {
                if (get_class($this->observers[$i]) == get_class($observer)) {
                    unset($this->observers[$i]);
                }
            }
        }
    }

    public function notify(): void
    {
        if (!empty($this->observers)) {
            foreach ($this->observers as $observer) {
                $observer->update($this);
            }
        }
    }

    public function getState(): ?string 
    {
        return $this->state;
    }

    public function setState(string $state): void 
    {
        $this->state = $state;
        $this->notify();
    }

    public function getObservers(): array
    {
        return $this->observers;
    }
}

// Publisher
class User extends Subject
{
    public function login(): void
    {
        $this->setState('login');
    }

    public function logout(): void
    {
        $this->setState('logout');
    }

    public function publishPost(): void 
    {
        $this->setState('publishPost');
    }
}

// Subscriber
class ModeratorObserver extends Observer
{
    public function login($subject) 
    {
        echo 'Logged in' . '<br>';//or riderect
    }

    public function logout($subject) 
    {
        echo 'Logged out' . '<br>';//or riderect
    }

    public function publishPost($subject) 
    {
        echo 'Published post' . '<br>';//or riderect
    }
}

$user = new User();
$mc = new ModeratorObserver();

$user->attach($mc);
$user->login();
$user->publishPost();
$user->logout();
var_dump($user->getObservers());
$user->detach($mc);
$user->login();
var_dump($user->getObservers());