<?php declare(strict_types=1);

/*
* Example of one of the creational patterns
*/
class Screen
{
    public function __construct()
    {
        echo 'Screen is here' . '<br>';
    }
}

class Speaker
{
    public function __construct()
    {
        echo 'Speaker is here' . '<br>';
    }
}

class KeyBord
{
    public function __construct()
    {
        echo 'Keyboard is here' . '<br>';
    }
}

class Battery
{
    public function __construct()
    {
        echo 'Battery is here' . '<br>';
    }
}

abstract class DigitalDevice
{
    protected $elements = [];
    protected $name;
    protected $model;
    protected $producer;

    protected function setPart(string $key, object $value): void
    {
        $this->elements[$key] = $value;
    }

    protected function getParts(): array
    {
        return $this->elements;
    }

    protected function getDeviceInformation(): string
    {
        return 'Name: ' . $this->name . ' Model: ' . $this->model . ' Producer: ' . $this->producer;
    }
}

class MobilePhoneBuilder
{
    private $phone;

    public function initializeDevice($name, $model, $producer): void
    {
        $this->phone = new MobilePhone($name, $model, $producer);
    }

    public function addScreen()
    {
        $this->phone->setPart('screen', new Screen());
    }

    public function addKeyBoard()
    {
        $this->phone->setPart('keyboard', new KeyBord());
    }

    public function addSpeaker()
    {
        $this->phone->setPart('speaker', new Speaker());
    }

    public function addBattery()
    {
        $this->phone->setPart('battery', new Battery());
    }

    public function getParts()
    {
        return $this->phone->getParts();
    }

    public function getDeviceInformation()
    {
        return $this->phone->getDeviceInformation();
    }

    public function getDevice(): DigitalDevice
    {
        return $this->phone;
    }
}

class LaptopBuilder
{
    private $phone;

    public function initializeDevice(string $name, string $model, string $producer): void
    {
        $this->phone = new Laptop($name, $model, $producer);
    }

    public function addScreen(): void
    {
        $this->phone->setPart('screen', new Screen());
    }

    public function addKeyBoard(): void
    {
        $this->phone->setPart('keyboard', new KeyBord());
    }

    public function addSpeaker(): void
    {
        $this->phone->setPart('speaker', new Speaker());
    }

    public function addBattery(): void
    {
        $this->phone->setPart('battery', new Battery());
    }

    public function getParts(): array
    {
        return $this->phone->getParts();
    }

    public function getDeviceInformation(): string
    {
        return $this->phone->getDeviceInformation();
    }

    public function getDevice(): DigitalDevice
    {
        return $this->phone;
    }
}

class MobilePhone extends DigitalDevice
{   
    public function __construct($name, $model, $producer)
    { 
        $this->name = $name;
        $this->model = $model;
        $this->producer = $producer;
    }

    public function setPart(string $key, object $value): void
    {
        parent::setPart($key, $value);
    }

    public function getParts(): array
    {
        return parent::getParts();
    }

    public function getDeviceInformation(): string
    {
        return parent::getDeviceInformation();
    }
}

class Laptop extends DigitalDevice
{
    public function __construct($name, $model, $producer)
    { 
        $this->name = $name;
        $this->model = $model;
        $this->producer = $producer;
    }

    public function setPart(string $key, object $value): void
    {
        parent::setPart($key, $value);
    }

    public function getParts(): array
    {
        return parent::getParts();
    }

    public function getDeviceInformation(): string
    {
        return parent::getDeviceInformation();
    }
}

class MainBuilder
{
    private $builder;

    public function __construct($builder)
    {
        $this->builder = $builder;
    }

    public function build($name, $model, $producer): DigitalDevice
    {
        $this->builder->initializeDevice($name, $model, $producer);
        $this->builder->addScreen();
        $this->builder->addKeyBoard();
        $this->builder->addSpeaker();
        $this->builder->addBattery();
       
        return $this->builder->getDevice();
    }

    public function deviceInformation()
    {
        print_r($this->builder->getDeviceInformation());
        print_r($this->builder->getParts());
    }
}

$mb = new MainBuilder(new MobilePhoneBuilder());
$mb->build('Samsung Galaxy', 'S 10', 'Samsung');
$mb->deviceInformation();
$mb = new MainBuilder(new LaptopBuilder());
$mb->build('ASUS ROG Strix', 'GL703GE-ES73 17.3', 'ASUS');
$mb->deviceInformation();