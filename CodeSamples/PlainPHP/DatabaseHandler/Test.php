<?php

/**
 * Simple Database Entity Manager with live example of
 * database installation and performance of the CRUD operations.
 */
include('config.php');
include('DatabaseConnector.php');
include('QueryException.php');
include('DynamicQueries.php');
include('Entity.php');
include('EntityManager.php');
include('BookManager.php');
include('Book.php');

use Config\Config;
use Database\DatabaseConnector;
use Queries\DynamicQueries;
//use EntityManager\EntityManager;
use EntityManager\BookManager;
use Models\Book;

function initBookTable(): void
{
    $database = new DatabaseConnector(Config::DB_DNS, Config::USER_NAME, Config::USER_PASSWORD);
    $query = new DynamicQueries($database);

    $table = 'book';
    $columns = ['id', 'customer_id', 'title', 'author'];
    $types = ['INT', 'INT', 'VARCHAR', 'VARCHAR'];
    $length = null; //['title' => 50, 'author' => 50];
    $attributes = ['PRIMARY KEY', 'NOT NULL', 'NOT NULL', 'NOT NULL'];
    $query->createTable($table, $columns, $types, $length, $attributes);
}

initBookTable();

$entityManager = new BookManager();
$book = new Book();

for ($i = 1; $i < 12; $i++) {
    $book->setId($i);
    $book->setCustomerId($i + 1);
    $book->setTitle('AddBookTest');
    $book->setAuthor('AddBookTest');

    $entityManager->add($book);
} 

$bookById = $entityManager->_findById(11);

if ($bookById) {
    echo $bookById->getBookDetails() . "\n";
}

$books = $entityManager->findAll();

foreach ($books as $a_Book) {
    echo $a_Book->getBookDetails() . "\n";
}

$book->setId(9);
$book->setCustomerId(4);
$book->setTitle('UpdateBookTest');
$book->setAuthor('UpdateBookTest');

$entityManager->modify($book);

$updatedBook = $entityManager->_findById(9);

echo $updatedBook->getBookDetails() . "\n";

$entityManager->erase($book);

$books = $entityManager->findAll();

foreach ($books as $a_Book) {
    echo $a_Book->getBookDetails() . "\n";
}
