<?php

declare(strict_types=1);

namespace Exceptions;

use Exception;

final class QueryException extends Exception
{
}