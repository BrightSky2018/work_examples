<?php

declare(strict_types=1);

namespace EntityManager;

/**
 * The class designed to manage models/entities. It provides
 * a generic set of database operation which can be used on entities of
 * different types. Thus, the class serves as a parent class that should be
 * extended by different EntityManager types, which would like to manage
 * a specific entity and to have an independent set of database operation.
 * 
 * NOTE: The BookManager is such example.
 */

use ReflectionProperty;
use Database\DatabaseConnector;
use Queries\DynamicQueries;
use EntityManager\IEntity;
use Config\Config;
use PDO;

abstract class EntityManager
{
    /**
     * @var string
     */
    protected $table;

    /**
     * @var array
     */
    protected $tableColumns;

    /**
     * @var DynamicQueries
     */
    protected $query;

    /**
     * @var DatabaseConnector
     */
    protected $database;

    /**
     * @var string
     */
    protected $entity;

    /**
     * @var array
     */
    protected $entities = [];

    public function __construct(string $entity, string $table)
    {
        $this->database = new DatabaseConnector(Config::DB_DNS, Config::USER_NAME, Config::USER_PASSWORD, [
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]);
        $this->query = new DynamicQueries($this->database);
        $this->tableColumns = $this->query->getColumnNames($table);
        $this->entity = $entity;
        $this->table = $table;
    }

    /**
     * Finds record by id and returns it as an object of the
     * corresponding entity type.
     * 
     * @param mixed
     * @return IEntity
     */
    public function findById($id): IEntity
    {
        $record = $this->query->select(
            $this->table,
            ['*'],
            [
                $this->query->getPrimaryKey($this->table) => $id
            ]
        )[0];

        $entity = new $this->entity();

        foreach ($this->tableColumns as $key => $property) {
            if (isset($record[$property])) {
                $this->accessEntityPropery($entity, $property)->setValue($entity, $record[$property]);
            }
        }

        return $entity;
    }

    /**
     * Find all records of the corresponding entity and returns
     * them as an array of entity type objects.
     * 
     * @return array
     */
    public function findAll(): array
    {
        $records = $this->query->select($this->table, ['*']);

        foreach ($records as $key => $record) {
            $entity = new $this->entity();

            foreach ($this->tableColumns as $column) {
                if (isset($record[$column])) {
                    $this->accessEntityPropery($entity, $column)->setValue($entity, $record[$column]);
                }
            }

            array_push($this->entities, $entity);
        }

        return $this->entities;
    }

    /**
     * Inserts a new entity into the database
     * 
     * @param IEntity
     * @return bool
     */
    public function add(IEntity $entity): bool
    {
        foreach ($this->tableColumns as $key => $column) {
            $reflection = $this->accessEntityPropery($entity, $column);
            $propValue = $reflection->getValue($entity);

            if ($propValue) {
                $data[$column] = $propValue;
            }
        }

        return $this->query->insert($this->table, $data);
    }

    /**
     * Updates specified, database entity record
     * 
     * @param IEntity
     * @return bool
     */
    public function modify(IEntity $entity): bool
    {
        foreach ($this->tableColumns as $key => $column) {
            $reflection = $this->accessEntityPropery($entity, $column);
            $propValue = $reflection->getValue($entity);

            if ($propValue) {
                $data[$column] = $propValue;
            }
        }

        $primaryValue = $data[$this->query->getPrimaryKey($this->table)];
        unset($data[$this->query->getPrimaryKey($this->table)]);

        return $this->query->update(
            $this->table,
            $data,
            [
                $this->query->getPrimaryKey($this->table) => $primaryValue
            ]
        );
    }

    /**
     * Deletes specified, database entity record
     * 
     * @param IEntity
     * @return bool
     */
    public function erase(IEntity $entity): bool
    {
        $delete = false;

        foreach ($this->tableColumns as $column) {
            $reflection = $this->accessEntityPropery($entity, $column);
            $propValue = $reflection->getValue($entity);

            if ($propValue) {
                if ($column == $this->query->getPrimaryKey($this->table)) {
                    $delete = $this->query->delete($this->table, [
                        $column => $propValue
                    ]);
                }
            }
        }

        return $delete;
    }

    /**
     * Allows to access entity's private properties
     * 
     * @param IEntity
     * @param string $property
     * @return ReflectionProperty
     */
    protected function accessEntityPropery(IEntity $entity, string $property): ReflectionProperty
    {
        $reflection = new ReflectionProperty($entity, $property);
        $reflection->setAccessible(true);

        return $reflection;
    }
}
