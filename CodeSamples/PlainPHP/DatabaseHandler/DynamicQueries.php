<?php

declare(strict_types=1);

namespace Queries;

use Database\DatabaseConnector;
use Exceptions\QueryException;
use Throwable;
use PDO;

final class DynamicQueries
{
    /**
     * @var PDO
     */
    private $connection;

    public function __construct(DatabaseConnector $database)
    {
        $this->connection = $database->connect();
    }

    /**
     * Creates new table
     * 
     * @param string $table
     * @param array $columns
     * @param array $types
     * @param array|null $length
     * @param array $attributes
     * @return bool
     * @throws QueryException
     */
    public function createTable(string $table, array $columns, array $types, ?array $length, array $attributes): bool
    {
        if (!$table || !$columns || !$types || !$attributes) {
            throw new QueryException('Table, columns, types and attributes can not be empty.');
        }

        $result = false;

        try {
            $this->connection->beginTransaction();

            $sql = "CREATE TABLE $table (";
            for ($i = 0; $i < count($columns); $i++) {
                if (isset($length[$columns[$i]])) {
                    $sql .= $columns[$i] . " " . $types[$i] . "(" . $length[$columns[$i]] . ") " . $attributes[$i];
                } else {
                    $sql .= $columns[$i] . " " . $types[$i] . " " . $attributes[$i];
                }

                if ($i < count($columns) - 1) {
                    $sql .= ", ";
                }
            }
            $sql .= ");";
            $stmt = $this->connection->prepare($sql);
            $result = $stmt->execute();

            $this->connection->commit();
        } catch (Throwable $e) {
            $this->connection->rollBack();
            throw new QueryException($e->getMessage());
        }

        return $result;
    }

    /**
     * Gets table's column names
     * 
     * @param string $table
     * @return array
     * @throws QueryException
     */
    public function getColumnNames(string $table): array
    {
        if (!$table) {
            throw new QueryException('Table argument can not be empty.');
        }

        $columns = [];

        try {
            $this->connection->beginTransaction();

            $sql = "SELECT column_name FROM information_schema.columns WHERE table_name = '$table'";
            $stmt = $this->connection->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll();
            $this->connection->commit();

            if ($result) {
                foreach ($result as $key => $data) {
                    $columns[] = $data['column_name'];
                }
            }
        } catch (Throwable $e) {
            $this->connection->rollBack();
            throw new QueryException($e->getMessage());
        }

        return $columns;
    }

    /**
     * Gets table's primary key
     * 
     * @param string $table
     * @return string
     * @throws QueryException
     */
    public function getPrimaryKey(string $table): string
    {
        if (!$table) {
            throw new QueryException('Table argument can not be empty.');
        }

        $result = '';

        try {
            $this->connection->beginTransaction();

            $sql = "SELECT c.column_name, c.ordinal_position
                FROM information_schema.key_column_usage AS c
                LEFT JOIN information_schema.table_constraints AS t
                ON t.constraint_name = c.constraint_name
                WHERE t.table_name = '$table' AND t.constraint_type = 'PRIMARY KEY'";
            $stmt = $this->connection->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll();
            $this->connection->commit();

            if ($result) {
                $result = $result[0]['column_name'];
            }
        } catch (Throwable $e) {
            $this->connection->rollBack();
            throw new QueryException($e->getMessage());
        }

        return $result;
    }

    /**
     * Selects database records
     * 
     * @param string $table
     * @param array $columns
     * @param array $condition
     * @return array
     * @throws QueryException
     */
    public function select(string $table, array $columns, array $condition = null): array
    {
        if (!$table || !$columns) {
            throw new QueryException('Table and columns arguments can not be empty.');
        }

        $fields = implode(", ", $columns);
        $result = [];

        try {
            $this->connection->beginTransaction();

            $sql = "SELECT " . $fields . " FROM " . $table . "";

            if (!is_null($condition)) {
                $keys = array_keys($condition);
                $values = array_values($condition);
                $sql .= " WHERE " . $keys[0] . " = :" . $keys[0] . "";
            }

            $stmt = $this->connection->prepare($sql);

            if (!is_null($condition)) {
                $stmt->bindValue(":" . $keys[0], $values[0], $this->parameterType($values[0]));
            }

            $stmt->execute();

            if (!$stmt->rowCount() > 0) {
                throw new QueryException('Could not select specified records.');
            }

            $result = $stmt->fetchAll();
            $this->connection->commit();
        } catch (Throwable $e) {
            $this->connection->rollBack();
            throw new QueryException($e->getMessage());
        }

        return $result;
    }

    /**
     * Joins tables and selects the results
     * 
     * @param array $tables
     * @param string $joinType
     * @param string $joinOn
     * @param array $condition
     * @return array
     * @throws QueryException
     */
    public function selectJoin(array $tables, string $joinType, string $joinOn, array $condition = null): array
    {
        if (!$tables || !$joinType || !$joinOn) {
            throw new QueryException('Tables, join type and join on arguments can not be empty.');
        }

        $mainTable = $tables[0];
        $result = [];

        try {
            $this->connection->beginTransaction();

            $sql = "SELECT * FROM " . $mainTable . " ";

            for ($i = 1; $i < count($tables); $i++) {
                $sql .= strtoupper($joinType) . " " . "$tables[$i] ON $tables[$i].$joinOn = $mainTable.$joinOn ";
            }

            if (!is_null($condition)) {
                $sql .= $condition['clouse'] . " " . $condition['fields'] . " " . $condition['order'] . " ";
            }

            $stmt = $this->connection->prepare($sql);
            $stmt->execute();

            if (!$stmt->rowCount() > 0) {
                throw new QueryException('Could not select specified records.');
            }

            $result = $stmt->fetchAll();
            $this->connection->commit();
        } catch (Throwable $e) {
            $this->connection->rollBack();
            throw new QueryException($e->getMessage());
        }

        return $result;
    }

    /**
     * Inserts a new database record
     * 
     * @param string $table
     * @param array $data
     * @return bool
     * @throws QueryException
     */
    public function insert(string $table, array $data): bool
    {
        if (!$table || !$data) {
            throw new QueryException('Table and data arguments can not be emtpy.');
        }

        $columns = [];
        $columnsBindings = [];

        try {
            $this->connection->beginTransaction();

            $sql = "INSERT INTO " . $table . " ";
            foreach ($data as $key => $value) {
                $columns[] = $key;
                $columnsBindings[] = ':' . $key;
            }

            $sql = $sql . "(" . implode(', ', $columns) . ") VALUES (" . implode(', ', $columnsBindings) . ")";
            $stmt = $this->connection->prepare($sql);

            foreach ($data as $key => $value) {
                $stmt->bindValue(":"  . $key, $value, $this->parameterType($value));
            }

            $stmt->execute();

            if (!$stmt->rowCount() > 0) {
                throw new QueryException('Could not insert new record.');
            }

            $this->connection->commit();
        } catch (Throwable $e) {
            $this->connection->rollBack();
            throw new QueryException($e->getMessage());
        }

        return true;
    }

    /**
     * Deletes a specified database record
     * 
     * @param string $table
     * @param array $column
     * @return bool
     * @throws QueryException
     */
    public function delete(string $table, array $column): bool
    {
        if (!$table || !$column) {
            throw new QueryException('Table and column arguments can not be empty.');
        }

        $keys = array_keys($column);
        $values = array_values($column);

        try {
            $this->connection->beginTransaction();

            $sql = "DELETE FROM " . $table . " WHERE " . $keys[0] . " = :" . $keys[0] . " ";
            $stmt = $this->connection->prepare($sql);
            $stmt->bindValue(':' . $keys[0], $values[0], $this->parameterType($values[0]));
            $stmt->execute();

            if (!$stmt->rowCount() > 0) {
                throw new QueryException('Could not delete specified record.');
            }

            $this->connection->commit();
        } catch (Throwable $e) {
            $this->connection->rollBack();
            throw new QueryException($e->getMessage());
        }

        return true;
    }

    /**
     * Updates a specified database record
     * 
     * @param string $table
     * @param array $data
     * @param array $column
     * @return bool
     * @throws QueryException
     */
    public function update(string $table, array $data, array $column): bool
    {
        if (!$table || !$data || !$column) {
            throw new QueryException('Table, data and column arguments must be filled.');
        }

        $keys = array_keys($column);
        $values = array_values($column);

        try {
            $this->connection->beginTransaction();

            $sql = "UPDATE " . $table . " SET ";
            $first = true;

            foreach (array_keys($data) as $key) {
                if ($first) {
                    $first = false;
                } else {
                    $sql .= ', ';
                }

                $value = ':' . $key;
                $sql .= $key;
                $sql .= ' = ';
                $sql .= $value;
            }

            $sql .= " WHERE " . $keys[0] . " = :" . $keys[0] . "";
            $stmt = $this->connection->prepare($sql);

            foreach ($data as $key => $value) {
                $stmt->bindValue(":" . $key, $value, $this->parameterType($value));
            }

            $stmt->bindValue(":" . $keys[0], $values[0], $this->parameterType($values[0]));
            $stmt->execute();

            if (!$stmt->rowCount() > 0) {
                throw new QueryException('Could not update specified records !');
            }

            $this->connection->commit();
        } catch (Throwable $e) {
            $this->connection->rollBack();
            throw new QueryException($e->getMessage());
        }

        return true;
    }

    /**
     * Binds values to their corresponding PDO param types
     * 
     * @param mixed $value
     * @param mixed $varType
     * @return int
     */
    private function parameterType($value, $varType = null): int
    {
        if (is_null($varType)) {
            switch (true) {
                case is_bool($value):
                    $varType = PDO::PARAM_BOOL;
                    break;
                case is_string($value):
                    $varType = PDO::PARAM_INT;
                    break;
                case is_null($value):
                    $varType = PDO::PARAM_NULL;
                    break;
                default:
                    $varType = PDO::PARAM_STR;
            }
        }

        return $varType;
    }
}
