<?php

declare(strict_types=1);

namespace Models;

use EntityManager\IEntity;

final class Book implements IEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $customer_id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $author;

    public function __construct()
    {
    }

    /**
     * Sets book id
     * 
     * @param int $id
     * @return void
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * Gets book id
     * 
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Sets customer id
     * 
     * @param int $customerId
     * @return void
     */
    public function setCustomerId(int $customerId): void
    {
        $this->customer_id = $customerId;
    }

    /**
     * Gets customer id
     * 
     * @return int
     */
    public function getCustomerId(): int
    {
        return $this->customer_id;
    }

    /**
     * Sets book title
     * 
     * @param string $title
     * @return void
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * Gets book title
     * 
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Sets book author
     * 
     * @param string $author
     * @return void
     */
    public function setAuthor(string $author): void
    {
        $this->author = $author;
    }

    /**
     * Gets book author
     * 
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * Returns full information about the book
     * 
     * @return string
     */
    public function getBookDetails(): string
    {
        return  $this->id . ' | ' . $this->customer_id . ' | ' . $this->title . ' | ' . $this->author;
    }
}
