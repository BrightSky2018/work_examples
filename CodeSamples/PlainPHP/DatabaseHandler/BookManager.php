<?php

declare(strict_types=1);

namespace EntityManager;

/**
 * Book manager class is a dedicated class to manage Book entities
 * and to keep all the queries related to the Book model in one place.
 * The class extend parent EntityManager class, thus derives curtain,
 * base functionality from there.
 */

use EntityManager\EntityManager;
use Models\Book;

final class BookManager extends EntityManager
{
    public function __construct()
    {
        parent::__construct(Book::class, 'book');
    }

    /**
     * Finds a specified Book by ID
     * 
     * @param int $id Book id to be found
     * @return Book
     */
    public function _findById(int $id): Book
    {
        return parent::findById($id);
    }
}