<?php

namespace Config;

final class Config
{
    const DB_DNS = "pgsql:host=localhost;port=5432;dbname=book_store";
    const USER_NAME = 'postgres';
    const USER_PASSWORD = 'postgres';
}