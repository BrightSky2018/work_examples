<?php

declare(strict_types=1);

namespace Database;

use PDO;
use Throwable;
use RuntimeException;

final class DatabaseConnector
{
    /**
     * @var string
     */
    private $dns;

    /**
     * @var string
     */
    private $dbuser;

    /**
     * @var string
     */
    private $dbpass;

    /**
     * @var array
     */
    private $dboptions;

    /**
     * @var PDO
     */
    private $connection;

    public function __construct(
        string $dns,
        string $dbuser,
        string $dbpass,
        array $dboptions = null
    ) {
        $this->dns = $dns;
        $this->dbuser = $dbuser;
        $this->dbpass = $dbpass;
        $this->dboptions = $dboptions;
    }

    public function __destruct()
    {
        $this->disconnect();
    }

    /**
     * Creates a new database connection
     * 
     * @return PDO
     * @throws RuntimeException
     */
    public function connect(): PDO
    {
        try {
            $this->connection = new PDO($this->dns, $this->dbuser, $this->dbpass, $this->dboptions);
        } catch (Throwable $e) {
            throw new RuntimeException(
                'The PDO exception was ' . $e->getMessage(),
                $e->getCode(),
                $e
            );
        }

        return $this->connection;
    }

    /**
     * Closes existing database connection
     * 
     * @return void
     */
    private function disconnect(): void
    {
        if (isset($this->connection)) {
            $this->connection = null;
        }
    }
}
