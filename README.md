### What is this repository about? ###

The repository contains some coding examples to provide one with the overall picture of my skills and knowledge. 
I possibly can not cover all programming topics and provide in depth examples for each of them. The examples are
just to project my knowledge of the concepts and ability to work with them.

## Data Structures

Datas tructures is a fundamental programming concept, which essentially says how to structure informatio/data into
discrete, independent and easy to operate blocks.

![alt text](data_structures.png)

## Design Patterns

Design patterns is another fundamental programming concept about organization of the code on the higher level of abstraction (essentially organizing data structures and algorithms in reusable blocks).
Design patters provide best practice solutions to typical software engineering problems, allows code to be reusable, easier maintained
and well organized.

![alt text](design_patterns.png)

## Algorithms

This part of the programming describes sequence of the most efficient actions with respect to the computing time/space complexity - denoted as O(n), when working with data/data structures. The most essential set of algorithms usually related to the most common operations which can be performed on data i.e - search, sort and insert, delete data elements.

![alt text](algorithms.png)